﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class ModelAndAnimationsImporter
{
	#region Internal
	private static int count = 0;

	private class Context
	{
		public string modelFilePath;
		public string modelFileName;
		public string animsFolderPath;
		public string modelName;

		public string tempFolderPath;

		public Context(string _modelFilePath,string _animsFolderPath)
		{
			Initialize(_modelFilePath,_animsFolderPath);
		}

		private void Initialize(string _modelFilePath,string _animsFolderPath)
		{
			count++;

			modelFilePath = _modelFilePath;
			modelFileName = Path.GetFileName(modelFilePath);
			animsFolderPath = _animsFolderPath;

			modelName = Path.GetFileNameWithoutExtension(modelFilePath);

			tempFolderPath = Path.GetTempPath() + Path.DirectorySeparatorChar + "tmpImportModel" + count;

			if(Directory.Exists(tempFolderPath))
				Directory.Delete(tempFolderPath,true);
			Directory.CreateDirectory(tempFolderPath);
		}

		public Context()
		{
			string _modelFilePath = EditorUtility.OpenFilePanel("Select model file...","","FBX");
			string _animsFolderPath = EditorUtility.OpenFolderPanel("Select animations folder...","","");

			Initialize(_modelFilePath,_animsFolderPath);
		}
	}
	private static void DoTempCopy(Context ctx)
	{
		File.Copy(ctx.modelFilePath,ctx.tempFolderPath + Path.DirectorySeparatorChar + ctx.modelFileName);

		foreach(string animFileName in Directory.GetFiles(ctx.animsFolderPath))
		{
			/*if(!Path.GetExtension(animFileName).ToLower().Equals("fbx"))
				continue;*/
			if(Path.GetFileName(animFileName).Equals(ctx.modelFileName))
				continue;

			string newFileName = ctx.modelName + "@" + Path.GetFileName(animFileName);
			File.Copy(animFileName,ctx.tempFolderPath + Path.DirectorySeparatorChar + newFileName);
		}
	}

	private static void DeletePreviousAssets(Context ctx,string assetFolderPath)
	{
		foreach(string filePath in Directory.GetFiles(assetFolderPath))
		{
			// Only delete existing assets. This preserves any later added assets to a model folder
			if(File.Exists(ctx.tempFolderPath + Path.DirectorySeparatorChar + Path.GetFileName(filePath)))
				AssetDatabase.DeleteAsset(filePath);
		}
	}

	private static void ImportAssets(Context ctx)
	{
		if(!Directory.Exists("Assets" + Path.DirectorySeparatorChar + "Models"))
			Directory.CreateDirectory("Assets" + Path.DirectorySeparatorChar + "Models");
		string assetFolderPath = "Assets" + Path.DirectorySeparatorChar + "Models" + Path.DirectorySeparatorChar +
			ctx.modelName;
		if(!Directory.Exists(assetFolderPath))
			Directory.CreateDirectory(assetFolderPath);
		else
			DeletePreviousAssets(ctx,assetFolderPath);

		foreach(string filePath in Directory.GetFiles(ctx.tempFolderPath))
		{
			string fileName = Path.GetFileName(filePath);

			File.Copy(filePath,assetFolderPath + Path.DirectorySeparatorChar + fileName);
		}
		foreach(string filePath in Directory.GetFiles(assetFolderPath))
		{
			AssetDatabase.ImportAsset(filePath);
		}
	}

	private static void DeleteTempFolder(Context ctx)
	{
		Directory.Delete(ctx.tempFolderPath,true);
	}

	private static void internalModelImport(Context ctx)
	{
		DoTempCopy(ctx);
		ImportAssets(ctx);
		DeleteTempFolder(ctx);
	}
	#endregion

	public static void ModelImport(string modelFilePath,string animsFolderPath)
	{
		Context ctx = new Context(modelFilePath,animsFolderPath);
		internalModelImport(ctx);
	}
	public static void ModelImport(string[] modelFilePaths,string animsFolderPath)
	{
		foreach(string modelFilePath in modelFilePaths)
		{
			ModelImport(modelFilePath,animsFolderPath);
		}
	}

	[MenuItem("Asset Management/Import Model and Animations")]
	public static void ModelImportGUI()
	{
		Context ctx = new Context(); // Create context and temp folder
		internalModelImport(ctx);
	}
	[MenuItem("Asset Management/Batch Import Models and Animations")]
	public static void ModelImportBatchGUI()
	{
		string animsFolderPath = EditorUtility.OpenFolderPanel("Select animations folder...","","");
		List<string> modelFilePathList = new List<string>();
		bool done = false;
		while(!done)
		{
			string path = EditorUtility.OpenFilePanel("Select model file...","","FBX");
			if(path == null || path.Length <= 0)
			{
				done = true;
				break;
			}
			modelFilePathList.Add(path);

			if(EditorUtility.DisplayDialog("Done?","Have you selected all models?","Yes","No"))
				done = true;
		}
		string[] modelFilePaths = modelFilePathList.ToArray();
		ModelImport(modelFilePaths,animsFolderPath);
	}
}