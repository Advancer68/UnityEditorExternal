﻿using UnityEngine;
using System.Collections;

public class MonoSingletonC<T>:MonoBehaviour where T:MonoSingletonC<T> {


	private static T m_Instance = null;
	private static object m_lock_instance = new object();

	public static T instance {
		get {
			if(m_Instance == null) {
				m_Instance = GameObject.FindObjectOfType(typeof(T)) as T;
				if(m_Instance == null) {
					m_Instance = new GameObject("Singleton of " + typeof(T).ToString(),typeof(T)).GetComponent<T>();
					m_Instance.Init();
				}

			}
			return m_Instance;
		}
	}

	protected virtual void Awake() {
		if(m_Instance == null) {
			m_Instance = this as T;
		}
	}

	public virtual void Init() { }


	protected virtual void OnApplicationQuit() {
		m_Instance = null;
	}
}
