﻿using UnityEngine;
using System.Collections;

public class PADebugTest : MonoBehaviour {
	public string path;
	public Object obj = null;
	public GameObject gobj = null;
	public GameObject parent = null;
	public AudioSource ss = null;
	// Use this for initialization
	int i = 0;
	void Awake()
	{
		//PADebug p = PADebug.instance;
	}
	void Start () {
		UIEventListener listener = UIEventListener.Get(gameObject);
		listener.onHover = _onHover;
	}
	void _onHover(GameObject g,bool b)
	{
		Debug.Log(string.Format("onhober is {0}",b));
	}
	[ContextMenu("Load")]
	void Load()
	{
		UnityEngine.Object obj = Resources.Load(path);
		gobj = obj as GameObject;
		gobj = NGUITools.AddChild(parent,gobj);
	}
	[ContextMenu("releaseGameObject")]
	void ReleaseGameObject()
	{
		GameObject.DestroyObject(gobj);
		gobj = null;
	}
	[ContextMenu("releasePrefab")]
	void ReleasePrefab()
	{
		obj = null;
	}
	[ContextMenu("UnloadUnused")]
	void UnloadUnused()
	{
		Resources.UnloadUnusedAssets();
	}
	[ContextMenu("test")]
	void test()
	{
		//PADebugMono pamono = PADebugMono.instance;
		//PADebug p = PADebug.instance;
		//PADebug.LogError("test1{0}",i++);
		//PADebug.LogError("test1{0}",i++);
		//PADebug.LogError("test1{0}",i++);
	}
	[ContextMenu("Block")]
	void block()
	{
		enabled = true;
	}
	[ContextMenu("UnBlock")]
	void unblock()
	{
		enabled = false;
	}
	// Update is called once per frame
	void Update () {
		//PADebug.LogError("test1{0}",i++);
		
	}
}
