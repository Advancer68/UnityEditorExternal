﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace PAExternal.StateMachine
{
	using ConditionMode = UnityEditorInternal.TransitionConditionMode;
	public class ParamObject:UnityEngine.Object
	{
		public virtual bool Check()
		{
			return false;
		}
	}
	public class ParamFloat:ParamObject
	{
		private float value;
		public override bool Check()
		{
			return base.Check();
		}
	}
	public class ParamInt:ParamObject
	{
		private int value;
	}
	public class ParamBool:ParamObject
	{
		private bool value;
	}
	public class PACondition:Object
	{
		protected List<PACondition> m_Children = new List<PACondition>();
		public ConditionMode mode;
		// Use this for initialization
		
		public PACondition AddCondition()
		{
			PACondition co = CreateCondition();
			m_Children.Add(co);
			return co;
		}
		public virtual bool Check()
		{
			return false;
		}
		private PACondition CreateCondition()
		{
			PACondition co = new PACondition();
			return co;
		}
	}
	public class ConditionAnd:PACondition
	{
		public override bool Check()
		{
			foreach(PACondition item in m_Children)
			{
				if(!item.Check())
				{
					return false;
				}
			}
			return true;
		}
	}
	public class ConditoinOr:PACondition
	{
		public override bool Check()
		{
			foreach(PACondition item in m_Children)
			{
				if(item.Check())
				{
					return true;
				}
			}
			return false;
		}
	}
}
