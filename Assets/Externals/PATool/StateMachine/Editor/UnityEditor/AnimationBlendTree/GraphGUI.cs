﻿namespace UnityEditor.Graphs.PAAnimationBlendTree
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEditor;
    using UnityEditor.Graphs;
    using UnityEditorInternal;
    using UnityEngine;

    internal class GraphGUI : UnityEditor.Graphs.GraphGUI
    {
        private float[] m_Weights = new float[0];

        private void DoDelete()
        {
            this.blendTreeGraph.RemoveNodeMotions(base.selection);
            this.blendTreeGraph.BuildFromBlendTree(this.blendTreeGraph.rootBlendTree);
        }

        private void HandleGraphInput()
        {
            Event current = Event.current;
            switch (current.type)
            {
                case EventType.ValidateCommand:
                    if ((current.commandName == "SoftDelete") || (current.commandName == "Delete"))
                    {
                        current.Use();
                    }
                    break;

                case EventType.ExecuteCommand:
                    if ((current.commandName == "SoftDelete") || (current.commandName == "Delete"))
                    {
                        this.DoDelete();
                        current.Use();
                    }
                    break;

                case EventType.MouseDown:
                    if ((current.button == 0) && ((Application.platform != RuntimePlatform.OSXEditor) || !current.control))
                    {
                        base.selection.Clear();
                        this.UpdateUnitySelection();
                    }
                    break;

                case EventType.KeyDown:
                    if (current.keyCode == KeyCode.Delete)
                    {
                        this.DoDelete();
                        current.Use();
                    }
                    break;
            }
        }

        private void HandleNodeInput(UnityEditor.Graphs.AnimationBlendTree.Node node)
        {
            Event current = Event.current;
            if ((current.type == EventType.MouseDown) && (current.button == 0))
            {
                base.selection.Clear();
                base.selection.Add(node);
                Selection.activeObject = node.motion;
                if (((current.clickCount == 2) && (node.motion is BlendTree)) && (this.blendTreeGraph.rootBlendTree != node.motion))
                {
                    base.selection.Clear();
                    this.m_Tool.AddBreadCrumb(node.motion);
                }
                current.Use();
            }
        }

        public override void NodeGUI(UnityEditor.Graphs.Node n)
        {
            UnityEditor.Graphs.AnimationBlendTree.Node node = n as UnityEditor.Graphs.AnimationBlendTree.Node;
            BlendTree motion = node.motion as BlendTree;
            GUILayoutOption[] options = new GUILayoutOption[] { GUILayout.Width(200f) };
            GUILayout.BeginVertical(options);
            IEnumerator<Slot> enumerator = n.inputSlots.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    Slot current = enumerator.Current;
                    base.LayoutSlot(current, current.title, false, false, false, UnityEditor.Graphs.Styles.varPinIn);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            IEnumerator<Slot> enumerator2 = n.outputSlots.GetEnumerator();
            try
            {
                while (enumerator2.MoveNext())
                {
                    Slot s = enumerator2.Current;
                    base.LayoutSlot(s, s.title, false, false, false, UnityEditor.Graphs.Styles.varPinOut);
                }
            }
            finally
            {
                if (enumerator2 == null)
                {
                }
                enumerator2.Dispose();
            }
            n.NodeUI(this);
            EditorGUIUtility.labelWidth = 50f;
#if false
            if (motion != null)
            {
                for (int i = 0; i < 1; i++)
                {
					/*
                    string recursiveBlendParameter = motion.GetRecursiveBlendParameter(i);
                    float recursiveBlendParameterMin = motion.GetRecursiveBlendParameterMin(i);
                    float recursiveBlendParameterMax = motion.GetRecursiveBlendParameterMax(i);*/
                    EventType type = Event.current.type;
                    if ((Event.current.button != 0) && Event.current.isMouse)
                    {
                        Event.current.type = EventType.Ignore;
                    }
                    /*EditorGUI.BeginChangeCheck();
					
                    float parameterValue = EditorGUILayout.Slider(recursiveBlendParameter, this.blendTreeGraph.GetParameterValue(recursiveBlendParameter), recursiveBlendParameterMin, recursiveBlendParameterMax, new GUILayoutOption[0]);
                    if (EditorGUI.EndChangeCheck())
                    {
                        this.blendTreeGraph.SetParameterValue(recursiveBlendParameter, parameterValue);
                        InspectorWindow.RepaintAllInspectors();
                    }*/
                    if (Event.current.button != 0)
                    {
                        Event.current.type = type;
                    }
                }
                if (node.animator != null)
                {
                    List<Edge> list = new List<Edge>(n.outputEdges);
                    node.UpdateAnimator();
                    if (this.m_Weights.Length != list.Count)
                    {
                        this.m_Weights = new float[list.Count];
                    }
                    BlendTreePreviewUtility.GetRootBlendTreeChildWeights(node.animator, 0, node.animator.GetCurrentAnimatorStateInfo(0).nameHash, this.m_Weights);
                    for (int j = 0; j < list.Count; j++)
                    {
                        UnityEditor.Graphs.AnimationBlendTree.Node node2 = list[j].toSlot.node as UnityEditor.Graphs.AnimationBlendTree.Node;
                        node2.weight = node.weight * this.m_Weights[j];
                        list[j].color = node2.weightEdgeColor;
                    }
                }
            }
#endif
            GUILayout.EndVertical();
            this.HandleNodeInput(n as UnityEditor.Graphs.AnimationBlendTree.Node);
        }

        public override void OnGraphGUI()
        {
            this.SyncGraphToUnitySelection();
            this.blendTreeGraph.PopulateParameterValues();
            base.m_Host.BeginWindows();
            foreach (UnityEditor.Graphs.AnimationBlendTree.Node node in base.graph.nodes)
            {
                c__AnonStorey12 storey = new c__AnonStorey12 {
                    f__this = this,
                    n2 = node
                };
                bool on = base.selection.Contains(node);
                UnityEngine.Color color = GUI.color;
                GUI.color = !on ? node.weightColor : UnityEngine.Color.white;
                GUILayoutOption[] options = new GUILayoutOption[] { GUILayout.Width(0f), GUILayout.Height(0f) };
                node.position = GUILayout.Window(node.GetInstanceID(), node.position, new GUI.WindowFunction(storey.m__2F), node.title, UnityEditor.Graphs.Styles.GetNodeStyle(node.style, node.color, on), options);
                GUI.color = color;
            }
            base.m_Host.EndWindows();
            this.edgeGUI.DoEdges();
            this.HandleGraphInput();
        }

        private void SyncGraphToUnitySelection()
        {
            if (GUIUtility.hotControl == 0)
            {
                base.selection.Clear();
                foreach (UnityEngine.Object obj2 in Selection.objects)
                {
                    UnityEditor.Graphs.AnimationBlendTree.Node item = null;
                    Motion motion = obj2 as Motion;
                    if (motion != null)
                    {
                        item = this.blendTreeGraph.FindNode(motion);
                    }
                    if (item != null)
                    {
                        base.selection.Add(item);
                    }
                }
            }
        }

        public UnityEditor.Graphs.PAAnimationBlendTree.Graph blendTreeGraph
        {
            get
            {
                return (base.graph as UnityEditor.Graphs.PAAnimationBlendTree.Graph);
            }
        }

		private PAStateMachineWindow m_Tool
        {
            get
            {
				return (base.m_Host as PAStateMachineWindow);
            }
        }

        [CompilerGenerated]
        private sealed class c__AnonStorey12
        {
            internal UnityEditor.Graphs.PAAnimationBlendTree.GraphGUI f__this;
            internal UnityEditor.Graphs.AnimationBlendTree.Node n2;

            internal void m__2F(int id)
            {
                this.f__this.NodeGUI(this.n2);
            }
        }
    }
}

