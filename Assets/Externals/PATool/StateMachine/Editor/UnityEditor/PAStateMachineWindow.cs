﻿// C# example:
//define BlendTreeInspector_h
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using UnityEditor.Graphs;

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
namespace UnityEditor
{
	internal class PAStateMachineWindow:EditorWindow
	{
		//BreadCrumb
		[Serializable]
		private class BreadCrumbElement
		{
			[SerializeField]
			private UnityEngine.Object m_Target;

			public BreadCrumbElement(UnityEngine.Object target)
			{
				this.m_Target = target;
			}

			public string name
			{
				get
				{
					return ((this.m_Target == null) ? string.Empty : this.m_Target.name);
				}
			}

			public UnityEngine.Object target
			{
				get
				{
					return this.m_Target;
				}
			}
		}
		[SerializeField]
		private List<BreadCrumbElement> m_BreadCrumbs;
		//style
		private class Styles
		{
			public readonly GUIContent addIcon = EditorGUIUtility.IconContent("Toolbar Plus");
			public readonly GUIStyle bottomBarDarkBg = "In BigTitle";
			public readonly GUIStyle breadCrumbLeft = "GUIEditor.BreadcrumbLeft";
			public readonly GUIStyle breadCrumbMid = "GUIEditor.BreadcrumbMid";
			public readonly GUIStyle eventsBox = "flow overlay box";
			public readonly GUIStyle eventsHeader = "flow overlay header lower left";
			public readonly GUIStyle elementBackground = new GUIStyle("RL Element");
			public readonly GUIStyle invisbleButton = "InvisibleButton";
			public readonly GUIStyle layerBox = "flow overlay box";
			public readonly GUIStyle layerFoldout = "flow overlay foldout";
			public readonly GUIStyle layerHeader = "flow overlay header upper left";
			public readonly GUIStyle liveLinkLabel = new GUIStyle("miniLabel");
			public readonly GUIStyle nameLabel = new GUIStyle("miniLabel");
			public readonly GUIStyle overlayArea = "flow overlay area left";
			public readonly GUIContent removeIcon = EditorGUIUtility.IconContent("Toolbar Minus");
			public readonly GUIContent[] vectorLabels = new GUIContent[] { new GUIContent(string.Empty),new GUIContent(string.Empty),new GUIContent(string.Empty) };


			public Styles()
			{
				this.nameLabel.alignment = TextAnchor.MiddleRight;
				this.nameLabel.padding = new RectOffset(0,0,0,0);
				this.nameLabel.margin = new RectOffset(0,0,0,0);
				this.liveLinkLabel.alignment = TextAnchor.MiddleLeft;
				this.liveLinkLabel.padding = new RectOffset(0,0,0,0);
				this.liveLinkLabel.margin = new RectOffset(0,0,0,0);
			}
		}
		private GUILayoutOption[] nullOption = new GUILayoutOption[0];
		private static Styles s_Styles;
		//self custom
		string myString = "Hello World";
		private Rect rect = new Rect(100,200,200,200);
		private Rect bgRect;
		//
		public static System.Action graphDirtyCallback;
		public static PAStateMachineWindow tool;
		private Vector2 m_LayerScroll;
		//
		private float toolbarHeght = 20f;
		///layer field字段
		private List<bool> m_LayerExpansion;
		private string[] m_LayerNames;
		[SerializeField]
		private int m_SelectedLayerIndex = 0;
		//state machine
		//[SerializeField]
		public UnityEditor.Graphs.PAStateMachine.Graph stateMachineGraph;
		//[SerializeField]
		public UnityEditor.Graphs.PAStateMachine.GraphGUI stateMachineGraphGUI;
		//[SerializeField]
		public UnityEditor.Graphs.PAAnimationBlendTree.Graph blendTreeGraph;
		//[SerializeField]
		public UnityEditor.Graphs.PAAnimationBlendTree.GraphGUI blendTreeGraphGUI;
		[SerializeField]
		private bool m_AutoLiveLink = true;
		#region preperty
		/// <summary>
		/// 是否连接运行
		/// </summary>
		public bool liveLink
		{
			get
			{
				return true;
			}
		}
		public PAStateMachineWindow()
		{
			tool = this;
		}
		public int selectedLayerIndex
		{
			get
			{
				return this.m_SelectedLayerIndex;
			}
		}
		[SerializeField]
		private AnimatorController m_AnimatorController;
		public AnimatorController animatorController
		{
			get
			{
				return this.m_AnimatorController;
			}
			set
			{
				this.m_AnimatorController = value;
				this.m_SelectedLayerIndex = 0;
				this.ResetBreadCrumbs();
				/*if((this.m_PreviewObject != null) && (AnimatorController.GetEffectiveAnimatorController(this.m_PreviewObject) != this.m_AnimatorController))
				{
					this.m_PreviewObject = null;
				}*/
			}
		}
		#endregion

		public void RebuildGraph()
		{
			this.stateMachineGraph.RebuildGraph();
			if(graphDirtyCallback != null)
			{
				graphDirtyCallback();
			}
		}

		[CompilerGenerated]
		private sealed class c__AnonStoreyF
		{
			internal UnityEngine.Object target;

			internal bool m__2A(BreadCrumbElement o)
			{
				return (o.target == this.target);
			}
		}

		public void GoToBreadCrumbTarget(UnityEngine.Object target)
		{
			c__AnonStoreyF yf = new c__AnonStoreyF
			{
				target = target
			};
			int num = this.m_BreadCrumbs.FindIndex(new Predicate<BreadCrumbElement>(yf.m__2A));
			while(this.m_BreadCrumbs.Count > (num + 1))
			{
				this.m_BreadCrumbs.RemoveAt(num + 1);
			}
			this.stateMachineGraphGUI.CenterGraph();
			this.blendTreeGraphGUI.CenterGraph();
		}
		private void DetectAnimatorControllerFromSelection()
		{
			AnimatorController activeObject = null;
			Debug.Log(Selection.activeObject);
			if((Selection.activeObject == null) && (this.animatorController == null))
			{
				this.animatorController = null;
			}
			if((Selection.activeObject is AnimatorController) && EditorUtility.IsPersistent(Selection.activeObject))
			{
				activeObject = Selection.activeObject as AnimatorController;
			}
			if(Selection.activeGameObject != null)
			{
				Animator component = Selection.activeGameObject.GetComponent<Animator>();
				if(component != null)
				{
					AnimatorController effectiveAnimatorController = AnimatorController.GetEffectiveAnimatorController(component);
					if(effectiveAnimatorController != null)
					{
						activeObject = effectiveAnimatorController;
					}
				}
			}
			bool flag = false;
			if((activeObject != null) && (activeObject != this.animatorController))
			{
				this.animatorController = activeObject;
				flag = true;
				if(this.animatorController == null)
				{
					return;
				}
			}
			if(((this.m_LayerExpansion != null) && (this.animatorController != null)) /*&& (flag || (this.m_LayerExpansion.Count != this.animatorController.layerCount))*/)
			{
				this.ResetLayerExpansionArray();
			}
		}
		#region layer view
		private void LayerView(EditorWindow host,Rect position)
		{
			GUILayout.BeginArea(position,s_Styles.overlayArea);
			//this.m_LayerScroll = GUILayout.BeginScrollView(this.m_LayerScroll,nullOption);
			EditorGUIUtility.labelWidth = 55;
			int layerCount = this.animatorController.layerCount;
			for(int i = 0,length = layerCount;i < length;i++)
			{
				if(BeginLayer(i))
				{
				}
				Endlayer();
			}
			int tag = m_SelectedLayerIndex;
			m_SelectedLayerIndex = EditorGUILayout.Popup(m_SelectedLayerIndex,this.m_LayerNames);
			if(tag != m_SelectedLayerIndex)
			{
				//this.m_SelectedLayerIndex = tag;
				this.ResetBreadCrumbs();
			}
			//GUILayout.EndScrollView();
			GUILayout.EndArea();
		}
		private bool BeginLayer(int layerIndex)
		{
			//EditorGUILayout.BeginFadeGroup(10/*this.animatorController.GetLayer(layerIndex).name,false*/);
			//EditorGUIUtility.
			//EditorGUI.Gr
			
            return this.m_LayerExpansion[layerIndex];
		}
		private void Endlayer()
		{
			//EditorGUILayout.BeginFadeGroup(11);
			//GUI.EndGroup();
		}
        private void OnControllerChange()
        {
#if false
            if ((this.m_PreviewObject != null) && (AnimatorController.GetEffectiveAnimatorController(this.m_PreviewObject) != this.animatorController))
            {
                this.animatorController = AnimatorController.GetEffectiveAnimatorController(this.m_PreviewObject);
                this.ResetLayerExpansionArray();
            }
#endif
			if(m_AnimatorController.layerCount != m_LayerExpansion.Count)
			{
				this.ResetLayerExpansionArray();
			}
        }
		private float PreCalcLayerViewHeight()
		{
			float a = 0f;
			for(int i = 0;i < this.animatorController.layerCount;i++)
			{
				if(this.m_LayerExpansion[i])
				{
					if(i == 0)
					{
						a += 43f;
					}
					else
					{
						a += 81f;
					}
				}
				else
				{
					a += 23f;
				}
			}
			a += 30f;
			return Mathf.Min(a,(base.position.height - toolbarHeght) * 0.5f);
		}
		private void ResetLayerExpansionArray()
		{
			this.m_LayerExpansion.Clear();
			if(this.animatorController != null)
			{
				this.m_LayerNames = null;
				this.m_LayerNames = new string[animatorController.layerCount];
				for(int i = 0;i < this.animatorController.layerCount;i++)
				{
					this.m_LayerExpansion.Add(false);
					this.m_LayerNames[i] = this.animatorController.GetLayer(i).name;
				}
			}
		}
		#endregion
		#region event view

		private float PreCalcEventViewHeight()
		{
			float a = (this.animatorController.parameterCount * 20f) + 26f;
			return Mathf.Min(a,(base.position.height - 17f) * 0.5f);
		}
		#endregion
		// Add menu named "My Window" to the Window menu
		[UnityEditor.MenuItem("Window/PAState Machine",false,0x7dd)]
		public static void DoWindow()
		{
			System.Type[] desiredDockNextTo = new System.Type[] { typeof(SceneView) };
			PAStateMachineWindow state = EditorWindow.GetWindow<PAStateMachineWindow>(/*desiredDockNextTo,*/"StateMachine");
			//state.ResetBreadCrumbs();
		}
		private void DoToolbar(Rect toolbarRect)
		{
			GUILayout.BeginArea(toolbarRect);
			GUILayout.BeginHorizontal(EditorStyles.toolbar,new GUILayoutOption[0]);
			GUILayout.Toggle(false,"GUILayout.Toggle,breadCrumbMid",s_Styles.breadCrumbMid /*s_Styles.breadCrumbLeft*/,new GUILayoutOption[0]);
			//GUILayout.Toggle(false,"GUILayout.Toggle,breadCrumbLeft",s_Styles.breadCrumbLeft,new GUILayoutOption[0]);
			//GUILayout.Button("breadCrumbLeft",s_Styles.breadCrumbLeft);
			GUILayout.FlexibleSpace();
			if(GUILayout.Button("Uptate",s_Styles.breadCrumbMid,nullOption))
			{
				this.animatorController = null;
				Debug.Log("uptate controller");
				this.DetectAnimatorControllerFromSelection();
			}
			this.m_AutoLiveLink = GUILayout.Toggle(this.m_AutoLiveLink,"Auto Live Link",EditorStyles.toolbarButton,new GUILayoutOption[0]);
			GUILayout.EndHorizontal();
			GUILayout.EndArea();
		}
		#region DrawGrid
		[SerializeField]
		protected Graph m_Graph;
		private static readonly UnityEngine.Color kGridMajorColorDark = new UnityEngine.Color(0f,0f,0f,0.28f);
		private static readonly UnityEngine.Color kGridMajorColorLight = new UnityEngine.Color(0f,0f,0f,0.15f);
		private static readonly UnityEngine.Color kGridMinorColorDark = new UnityEngine.Color(0f,0f,0f,0.18f);
		private static readonly UnityEngine.Color kGridMinorColorLight = new UnityEngine.Color(0f,0f,0f,0.1f);

		private static Material s_HandleWireMaterial;
		private static Material s_HandleWireMaterial2D;
		public static Material handleWireMaterial
		{
			get
			{
				if(s_HandleWireMaterial2D == null)
				{
					s_HandleWireMaterial2D = EditorGUIUtility.LoadRequired("SceneView/2DHandleLines.mat") as Material;
				}
				return s_HandleWireMaterial2D;
			}
		}
		public Graph graph
		{
			get
			{
				return this.m_Graph;
			}
			set
			{
				this.m_Graph = value;
			}
		}

		private static UnityEngine.Color gridMajorColor
		{
			get
			{
				if(EditorGUIUtility.isProSkin)
				{
					return kGridMajorColorDark;
				}
				return kGridMajorColorLight;
			}
		}

		private static UnityEngine.Color gridMinorColor
		{
			get
			{
				return kGridMinorColorDark;
			}
		}

		private void DrawGrid()
		{
			if(Event.current.type == EventType.Repaint)
			{
				Profiler.BeginSample("DrawGrid");
				handleWireMaterial.SetPass(0);//HandleUtility.
				GL.PushMatrix();
				GL.Begin(1);
				this.DrawGridLines(bgRect,12f,gridMinorColor);
				this.DrawGridLines(bgRect,120f,gridMajorColor);
				GL.End();
				GL.PopMatrix();
				Profiler.EndSample();
			}
		}
		private void DrawGridLines(Rect rect,float gridSize,UnityEngine.Color gridColor)
		{
			GL.Color(gridColor);
			for(float i = rect.xMin - (rect.xMin % gridSize);i < rect.xMax;i += gridSize)
			{
				this.DrawLine(new Vector2(i,rect.yMin),new Vector2(i,rect.yMax));
			}
			GL.Color(gridColor);
			for(float j = rect.yMin - (rect.yMin % gridSize);j < rect.yMax;j += gridSize)
			{
				this.DrawLine(new Vector2(rect.xMin,j),new Vector2(rect.xMax,j));
			}
		}

		private void DrawLine(Vector2 p1,Vector2 p2)
		{
			GL.Vertex((Vector3)p1);
			GL.Vertex((Vector3)p2);
		}
		#endregion
		#region DrawEventView
		private void DrawEventView(Rect position)
		{
			GUILayout.BeginArea(position,s_Styles.overlayArea);//开始绘制一个区域
			GUILayout.FlexibleSpace();
			if(!this.liveLink)
			{
				GUILayout.BeginHorizontal("Paramters",s_Styles.eventsHeader,nullOption);
			}
			else
			{
				GUILayout.BeginHorizontal("Paramters [Connected]",s_Styles.eventsHeader,nullOption);
			}
			GUILayout.FlexibleSpace();
			Rect rect =GUILayoutUtility.GetRect(s_Styles.addIcon,s_Styles.invisbleButton);
			//Debug.Log(rect);
			//Debug.Log(Event.current);
			if(EditorGUI_EX.ButtonMouseDown(rect,s_Styles.addIcon,FocusType.Passive,s_Styles.invisbleButton))
			{
				GenericMenu menu = new GenericMenu();
				IEnumerator enumerator = Enum.GetValues(typeof(AnimatorControllerParameterType)).GetEnumerator();
				try
				{
					while(enumerator.MoveNext())
					{
						object obj = enumerator.Current;
						menu.AddItem(new GUIContent(obj.ToString()),false,new GenericMenu.MenuFunction2(this.AddEventMenu),obj);
					}
				}
				finally
				{
					IDisposable disposable = enumerator as IDisposable;
					if(disposable != null)
					{
						disposable.Dispose();
					}
				}
				menu.DropDown(rect);
			}
			GUILayout.EndHorizontal();
			GUILayout.EndArea();
		}
		private void AddEventMenu(object value)
		{
		}
		#endregion
		#region StateMachineView

		private StateMachine GetParentStateMachine()
		{
			if(this.m_BreadCrumbs.Count == 1)
			{
				return null;
			}
			return (this.m_BreadCrumbs[this.m_BreadCrumbs.Count - 2].target as StateMachine);
		}
		private void StateMachineView(Rect position)
		{
			this.stateMachineGraph.rootStateMachine = this.m_BreadCrumbs.First<BreadCrumbElement>().target as StateMachine;
			this.stateMachineGraph.parentStateMachine = this.GetParentStateMachine();
			this.stateMachineGraph.activeStateMachine = this.m_BreadCrumbs.Last<BreadCrumbElement>().target as StateMachine;
			this.stateMachineGraphGUI.BeginGraphGUI(this,position);
			this.stateMachineGraphGUI.OnGraphGUI();
			this.stateMachineGraphGUI.EndGraphGUI();
		}
		#endregion
		#region BlendTreeView

		private void BlendTreeView(Rect position)
		{
			//this.blendTreeGraph.previewAvatar = this.m_PreviewObject;
			//this.blendTreeGraph.rootBlendTree = this.m_BreadCrumbs.Last<BreadCrumbElement>().target as BlendTree;
			this.blendTreeGraphGUI.BeginGraphGUI(this,position);
			this.blendTreeGraphGUI.OnGraphGUI();
			this.blendTreeGraphGUI.EndGraphGUI();
		}
		#endregion

		private static bool ShouldCaptureEvent(Rect eventView,Rect layerView)
		{
			if(!layerView.Contains(Event.current.mousePosition) && !eventView.Contains(Event.current.mousePosition))
			{
				return false;
			}
			EventType type = Event.current.type;
			switch(type)
			{
				case EventType.MouseDown:
				case EventType.ScrollWheel:
					return true;
			}
			return (EditorGUIUtility.editingTextField && (type == EventType.KeyDown));
		}
		#region BreadCrumbs
		private void ResetBreadCrumbs()
		{
			this.m_BreadCrumbs.Clear();
			if(((this.animatorController != null)) && ((this.m_SelectedLayerIndex >= 0) && (this.m_SelectedLayerIndex < this.animatorController.layerCount)))
			{
				StateMachine layerStateMachine = this.animatorController.GetLayer(this.m_SelectedLayerIndex).stateMachine;//.GetLayerStateMachine(this.m_SelectedLayerIndex);
				if(layerStateMachine != null)
				{
					this.stateMachineGraphGUI.ClearSelection();
					this.AddBreadCrumb(layerStateMachine);
					base.Repaint();
				}
			}
		}
		public void AddBreadCrumb(UnityEngine.Object target)
		{
			this.m_BreadCrumbs.Add(new BreadCrumbElement(target));
			this.stateMachineGraphGUI.CenterGraph();
			this.blendTreeGraphGUI.CenterGraph();
		}
		#endregion
		void OnEnable()
		{
			this.Init();
		}
		private void Init()
		{
			if(stateMachineGraph == null)
			{
				stateMachineGraph = ScriptableObject.CreateInstance<UnityEditor.Graphs.PAStateMachine.Graph>();
			}
			if(stateMachineGraphGUI == null)
			{
				stateMachineGraphGUI = stateMachineGraph.GetEditor() as UnityEditor.Graphs.PAStateMachine.GraphGUI;
			}
			if(blendTreeGraph == null)
			{
				blendTreeGraph = ScriptableObject.CreateInstance<UnityEditor.Graphs.PAAnimationBlendTree.Graph>();
			}
			if(blendTreeGraphGUI == null)
			{
				blendTreeGraphGUI = blendTreeGraph.GetEditor() as UnityEditor.Graphs.PAAnimationBlendTree.GraphGUI;
			}
			if(this.m_BreadCrumbs == null)
			{
				this.m_BreadCrumbs = new List<BreadCrumbElement>();
				this.ResetBreadCrumbs();
			}
			if(this.m_LayerExpansion == null)
			{
				this.m_LayerExpansion = new List<bool>();
			}
			if(this.m_LayerNames == null)
			{
				this.m_LayerNames = new string[]{};
			}
		}
		public void OnFocus()
		{
			//this.DetectAnimatorControllerFromSelection();
		}
		void OnGUI()
		{
			if(s_Styles == null)
			{
				s_Styles = new Styles();
			}
			DoToolbar(new Rect(0,0,base.position.width,toolbarHeght));
			if(this.animatorController != null)
			{
				OnControllerChange();
				if(false/*this.animatorController.isAssetBundled*/)
				{
					
				}
				else
				{
					Rect position = new Rect(0f,toolbarHeght,base.position.width,base.position.height - toolbarHeght);
					Rect layerView = new Rect(0f,toolbarHeght,100f,this.PreCalcLayerViewHeight());
					float height = this.PreCalcEventViewHeight();
					Rect eventView = new Rect(0f,(base.position.height - height) - 17f,220f,height);
					Rect nameRect = new Rect(0f,base.position.height - 24f,base.position.width,24f);
					EventType type = Event.current.type;
					if(ShouldCaptureEvent(eventView,layerView))
					{
						Event.current.type = EventType.Ignore;
					}
#if BlendTreeInspector_h
					BlendTreeInspector.parentBlendTree = null;
#endif
					if(this.m_BreadCrumbs.Count > 0)
					{
						if(this.m_BreadCrumbs.Last<BreadCrumbElement>().target is StateMachine)
						{
							this.StateMachineView(position);
						}
						if(this.m_BreadCrumbs.Last<BreadCrumbElement>().target is BlendTree)
						{
							this.BlendTreeView(position);
#if BlendTreeInspector_h
							BlendTreeInspector.parentBlendTree = this.m_BreadCrumbs.Last<BreadCrumbElement>().target as BlendTree;
#endif
						}
					}
					if(Event.current.type != EventType.Used)
					{
						Event.current.type = type;
					}
					LayerView(this,layerView);
					DrawEventView(eventView);
				}
			}
			//BeginWindows();
			//rect = GUI.Window(0,rect,WindowCall,"window");
			//EndWindows();


		}
		void WindowCall(int id)
		{
			//Debug.Log(id.ToString());
			GUI.skin.button.wordWrap = true;
			GUILayout.Button("button");
			GUI.Button(new Rect(20,20,30,30),"button");
			bool b = false;//GUI.RepeatButton(new Rect(10,10,30,30),"repeat");
			if(b)
			{
				Debug.Log("click gui preat");
			}
			b = GUILayout.RepeatButton("GUILayout RepeatButton");

			GUI.DragWindow();
		}
	}
}