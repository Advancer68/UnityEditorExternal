﻿namespace UnityEditor
{
    using System;
    using System.Collections.Generic;
    using UnityEditorInternal;
    using UnityEngine;

    internal class MecanimUtilities
    {
        internal static bool AreSameAsset(UnityEngine.Object obj1, UnityEngine.Object obj2)
        {
            return (AssetDatabase.GetAssetPath(obj1) == AssetDatabase.GetAssetPath(obj2));
        }

        internal static void DestroyBlendTreeRecursive(BlendTree blendTree)
        {
            for (int i = 0; i < blendTree.childCount; i++)
            {
                BlendTree motion = blendTree.GetMotion(i) as BlendTree;
                if ((motion != null) && AreSameAsset(blendTree, motion))
                {
                    DestroyBlendTreeRecursive(motion);
                }
            }
            Undo.DestroyObjectImmediate(blendTree);
        }

        internal static void DestroyStateMachineRecursive(StateMachine stateMachine)
        {
            for (int i = 0; i < stateMachine.stateMachineCount; i++)
            {
                StateMachine machine = stateMachine.GetStateMachine(i);
                if (AreSameAsset(stateMachine, machine))
                {
                    DestroyStateMachineRecursive(machine);
                }
            }
            for (int j = 0; j < stateMachine.stateCount; j++)
            {
                for (int k = 0; k < stateMachine.motionSetCount; k++)
                {
					BlendTree motionInternal = null;//stateMachine.GetState(j).GetMotion(k) as BlendTree;
                    if ((motionInternal != null) && AreSameAsset(stateMachine, motionInternal))
                    {
                        DestroyBlendTreeRecursive(motionInternal);
                    }
                }
            }
            Undo.DestroyObjectImmediate(stateMachine);
        }

        public static bool HasChildMotion(Motion parent, Motion motion)
        {
            if (parent == motion)
            {
                return true;
            }
            if (parent is BlendTree)
            {
                BlendTree tree = parent as BlendTree;
                int childCount = tree.childCount;
                for (int i = 0; i < childCount; i++)
                {
                    if (HasChildMotion(tree.GetMotion(i), motion))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool StateMachineRelativePath(StateMachine parent, StateMachine toFind, ref List<StateMachine> hierarchy)
        {
            hierarchy.Add(parent);
            if (parent == toFind)
            {
                return true;
            }
            for (int i = 0; i < parent.stateMachineCount; i++)
            {
                if (StateMachineRelativePath(parent.GetStateMachine(i), toFind, ref hierarchy))
                {
                    return true;
                }
            }
            hierarchy.Remove(parent);
            return false;
        }
    }
}

