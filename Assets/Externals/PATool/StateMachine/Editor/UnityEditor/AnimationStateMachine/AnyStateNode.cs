﻿namespace UnityEditor.Graphs.PAStateMachine
{
    using NUnit.Framework;
    using System;
    using System.Linq;
    using UnityEditor;
    using UnityEditor.Graphs;
    using UnityEngine;

	internal class AnyStateNode:UnityEditor.Graphs.PAStateMachine.Node
    {
        private void MakeTransitionCallback()
        {
            base.graphGUI.edgeGUI.BeginSlotDragging(base.outputSlots.First<Slot>(), true, false);
        }

        public override void NodeUI(UnityEditor.Graphs.GraphGUI host)
        {
			base.graphGUI = host as UnityEditor.Graphs.PAStateMachine.GraphGUI;
            Assert.NotNull(base.graphGUI);
            Event current = Event.current;
			if(UnityEditor.Graphs.PAStateMachine.Node.IsRightClick())
            {
                GenericMenu menu = new GenericMenu();
                menu.AddItem(new GUIContent("Make Transition"), false, new GenericMenu.MenuFunction(this.MakeTransitionCallback));
                menu.ShowAsContext();
                current.Use();
            }
        }

        public override void OnDrag()
        {
            base.OnDrag();
			PAStateMachineWindow.tool.stateMachineGraph.activeStateMachine.anyStatePosition = (Vector3)new Vector2(this.position.x,this.position.y);
        }

        public override UnityEngine.Object selectionObject
        {
            get
            {
                return this;
            }
        }

        public override UnityEngine.Object undoableObject
        {
            get
            {
				return PAStateMachineWindow.tool.stateMachineGraph.activeStateMachine;
            }
        }
    }
}

