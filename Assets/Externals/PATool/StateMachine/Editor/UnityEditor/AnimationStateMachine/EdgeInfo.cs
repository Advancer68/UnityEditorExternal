﻿namespace UnityEditor.Graphs.PAStateMachine
{
    using System;
    using System.Collections.Generic;
    using UnityEditorInternal;

    internal class EdgeInfo
    {
        public readonly List<Transition> transitions = new List<Transition>();

        public EdgeInfo(Transition transition)
        {
            this.transitions.Add(transition);
        }

        public EdgeDebugState debugState
        {
            get
            {
                int num = 0;
                int num2 = 0;
                foreach (Transition transition in this.transitions)
                {
                    if (transition.mute)
                    {
                        num++;
                    }
                    else if (transition.solo)
                    {
                        num2++;
                    }
                }
                if (num == this.transitions.Count)
                {
                    return EdgeDebugState.MuteAll;
                }
                if (num2 == this.transitions.Count)
                {
                    return EdgeDebugState.SoloAll;
                }
                if ((num > 0) && (num2 > 0))
                {
                    return EdgeDebugState.MuteAndSolo;
                }
                if (num > 0)
                {
                    return EdgeDebugState.MuteSome;
                }
                if (num2 > 0)
                {
                    return EdgeDebugState.SoloSome;
                }
                return EdgeDebugState.Normal;
            }
        }

        public bool hasMultipleTransitions
        {
            get
            {
                return (this.transitions.Count > 1);
            }
        }
    }
}

