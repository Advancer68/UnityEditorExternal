﻿using UnityEngine;
using System.Collections;
namespace UnityEditorInternal
{
	using NUnit.Framework;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Runtime.CompilerServices;
	using System.Runtime.InteropServices;
	using UnityEditor;
	using UnityEngine;
	public class StateMachineEX
	{
		public static State CreateState(StateMachine self,Vector3 position)
		{
			Undo.RegisterCompleteObjectUndo(self,"State added");
			State state = self.AddState("New State");
			state.position = position;
			return state;
		}
		public static bool HasState(StateMachine self,State state)
		{
			c__AnonStorey1F storeyf = new c__AnonStorey1F
			{
				state = state
			};
			return statesRecursive(self).Any<State>(new Func<State,bool>(storeyf.m__34));
		}
		public static bool HasState(StateMachine self,State state,bool recursive)
		{
			if(recursive)
			{
				return HasState(self,state);
			}
			else
			{
				List<State> l = new List<State>(states(self));
				return l.Contains(state);
			}
		}
		public static bool HasStateMachine(StateMachine self,StateMachine child)
		{
			c__AnonStorey21 storey = new c__AnonStorey21
			{
				child = child
			};
			return stateMachinesRecursive(self).Any<StateMachine>(new Func<StateMachine,bool>(storey.m__36));
		}
		public static bool HasStateMachine(StateMachine self,StateMachine child,bool recursive)
		{
			if(recursive)
			{
				return HasStateMachine(self,child);
			}
			else
			{
				List<StateMachine> l = new List<StateMachine>(stateMachines(self));
				return l.Contains(child);
			}
		}
		public static List<StateMachine> stateMachinesRecursive(StateMachine self)
		{
			List<StateMachine> list = new List<StateMachine>();
			list.AddRange(stateMachines(self));
			for(int i = 0;i < self.stateMachineCount;i++)
			{
				list.AddRange(stateMachinesRecursive(self.GetStateMachine(i)));
			}
			return list;
		}
		public static IEnumerable<StateMachine> stateMachines(StateMachine self)
		{
			StateMachine[] machineArray = new StateMachine[self.stateMachineCount];
			for(int i = 0;i < self.stateMachineCount;i++)
			{
				machineArray[i] = self.GetStateMachine(i);
			}
			return machineArray;
		}
		public static State FindState(StateMachine self,int stateUniqueNameHash)
		{
			c__AnonStorey1C storeyc = new c__AnonStorey1C
			{
				stateUniqueNameHash = stateUniqueNameHash
			};
			return statesRecursive(self).Find(new Predicate<State>(storeyc.m__31));
		}
		public static Vector3 GetStateMachinePosition(StateMachine self,StateMachine stateMachine)
		{
			for(int i = 0;i < self.stateMachineCount;i++)
			{
				if(stateMachine == self.GetStateMachine(i))
				{
					return self.GetStateMachinePosition(i);
				}
			}
			Assert.Fail("Can't find state machine (" + stateMachine.name + ") in parent state machine (" + self.name + ").");
			return Vector3.zero;
		}
		public static bool IsDirectParent(StateMachine self,StateMachine stateMachinel)
		{
			c__AnonStorey20 storey = new c__AnonStorey20
			{
				stateMachine = stateMachinel
			};
			return stateMachines(self).Any<StateMachine>(new Func<StateMachine,bool>(storey.m__35));
		}
		public static IEnumerable<State> states(StateMachine self)
		{
			State[] stateArray = new State[self.stateCount];
			for(int i = 0;i < self.stateCount;i++)
			{
				stateArray[i] = self.GetState(i);
			}
			return stateArray;
		}
		public static List<State> statesRecursive(StateMachine self)
		{
			List<State> list = new List<State>();
			list.AddRange(states(self));
			for(int i = 0;i < self.stateMachineCount;i++)
			{
				list.AddRange(statesRecursive(self.GetStateMachine(i)));
			}
			return list;
		}
		public static int transitionCount(StateMachine self)
		{
			List<Transition> t = transitions(self);
			return Mathf.Max(t.Count - 1,0);
		}

		public static void SetStateMachinePosition(StateMachine self,StateMachine stateMachine,Vector3 position)
		{
			for(int i = 0;i < self.stateMachineCount;i++)
			{
				if(stateMachine == self.GetStateMachine(i))
				{
					self.SetStateMachinePosition(i,position);
					return;
				}
			}
			Assert.Fail("Can't find state machine (" + stateMachine.name + ") in parent state machine (" + self.name + ").");
		}
		public static List<Transition> transitions(StateMachine self)
		{
			List<State> statesRecursives = statesRecursive(self);
			List<Transition> list2 = new List<Transition>();
			foreach(State state in statesRecursives)
			{
				list2.AddRange(self.GetTransitionsFromState(state));
			}
			list2.AddRange(self.GetTransitionsFromState(null));
			return list2;
		}
		[CompilerGenerated]
		private sealed class c__AnonStorey1B
		{
			internal string stateUniqueName;

			internal bool m__30(State s)
			{
				return (s.uniqueName == this.stateUniqueName);
			}
		}

		[CompilerGenerated]
		private sealed class c__AnonStorey1C
		{
			internal int stateUniqueNameHash;

			internal bool m__31(State s)
			{
				return (s.uniqueNameHash == this.stateUniqueNameHash);
			}
		}

		[CompilerGenerated]
		private sealed class c__AnonStorey1D
		{
			internal string transitionUniqueName;

			internal bool m__32(Transition t)
			{
				return (t.uniqueName == this.transitionUniqueName);
			}
		}

		[CompilerGenerated]
		private sealed class c__AnonStorey1E
		{
			internal int transitionUniqueName;

			internal bool m__33(Transition t)
			{
				return (t.uniqueNameHash == this.transitionUniqueName);
			}
		}

		[CompilerGenerated]
		private sealed class c__AnonStorey1F
		{
			internal State state;

			internal bool m__34(State s)
			{
				return (s == this.state);
			}
		}

		[CompilerGenerated]
		private sealed class c__AnonStorey21
		{
			internal StateMachine child;

			internal bool m__36(StateMachine sm)
			{
				return (sm == this.child);
			}
		}

		[CompilerGenerated]
		private sealed class c__AnonStorey22
		{
			internal State stateA;
			internal State stateB;

			internal bool m__37(Transition t)
			{
				return (t.dstState == this.stateB);
			}

			internal bool m__38(Transition t)
			{
				return (t.dstState == this.stateA);
			}
		}

		[CompilerGenerated]
		private sealed class c__AnonStorey20
		{
			internal StateMachine stateMachine;

			internal bool m__35(StateMachine sm)
			{
				return (sm == this.stateMachine);
			}
		}
	}
}
