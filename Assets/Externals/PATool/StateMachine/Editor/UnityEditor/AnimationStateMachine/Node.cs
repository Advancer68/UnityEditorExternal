﻿namespace UnityEditor.Graphs.PAStateMachine
{
    using System;
    using UnityEditor;
    using UnityEditor.Graphs;
    using UnityEngine;

    internal class Node : UnityEditor.Graphs.Node
    {
		protected UnityEditor.Graphs.PAStateMachine.GraphGUI graphGUI;

        public override void BeginDrag()
        {
            base.BeginDrag();
            Undo.RegisterCompleteObjectUndo(this.undoableObject, "Moved " + this.title);
        }

        protected static bool IsDoubleClick()
        {
            Event current = Event.current;
            return (((current.type == EventType.MouseDown) && (current.button == 0)) && (current.clickCount == 2));
        }

        protected static bool IsLeftClick()
        {
            Event current = Event.current;
            return (((current.type == EventType.MouseDown) && (current.button == 0)) && (current.clickCount == 1));
        }

        protected static bool IsRightClick()
        {
            return (Event.current.type == EventType.ContextClick);
        }

        public virtual UnityEngine.Object selectionObject
        {
            get
            {
                return null;
            }
        }

        public virtual UnityEngine.Object undoableObject
        {
            get
            {
                return null;
            }
        }
    }
}

