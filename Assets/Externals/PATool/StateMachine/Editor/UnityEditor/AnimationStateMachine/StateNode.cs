﻿namespace UnityEditor.Graphs.PAStateMachine
{
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEditor;
    using UnityEditor.Graphs;
    using UnityEditorInternal;
    using UnityEngine;

	internal class StateNode:UnityEditor.Graphs.PAStateMachine.Node
    {
        public UnityEditorInternal.State state;

        private void AddNewBlendTreeCallback()
        {
#if false
            BlendTree motionInternal = this.state.GetMotionInternal(PAStateMachineWindow.tool.motionSetIndex) as BlendTree;
            StateMachine layerStateMachine = PAStateMachineWindow.tool.animatorController.GetLayerStateMachine(PAStateMachineWindow.tool.selectedLayerIndex);
            bool flag = true;
            if (motionInternal != null)
            {
                string title = "This will delete current BlendTree in state.";
                string message = "You cannot undo this action.";
                if (EditorUtility.DisplayDialog(title, message, "Delete", "Cancel"))
                {
                    MecanimUtilities.DestroyBlendTreeRecursive(motionInternal);
                }
                else
                {
                    flag = false;
                }
            }
            else
            {
                Undo.RegisterCompleteObjectUndo(layerStateMachine, "Blend Tree Added");
            }
            if (flag)
            {
                BlendTree tree2 = this.state.CreateBlendTreeInternal(PAStateMachineWindow.tool.motionSetIndex);
                tree2.name = "Blend Tree";
                tree2.blendParameter = PAStateMachineWindow.tool.animatorController.GetDefaultBlendTreeParameter();
                tree2.blendParameterY = PAStateMachineWindow.tool.animatorController.GetDefaultBlendTreeParameter();
            }
#endif
        }

        private void CopyStateCallback()
        {
            Unsupported.CopyStateToPasteboard(this.state, PAStateMachineWindow.tool.animatorController);
        }

        public void DeleteStateCallback()
        {
            if (!base.graphGUI.selection.Contains(this))
            {
                base.graphGUI.selection.Add(this);
            }
            base.graphGUI.DeleteSelection();
            PAStateMachineWindow.tool.RebuildGraph();
        }

        public override void EndDrag()
        {
            base.EndDrag();
            StateMachine hoveredStateMachine = PAStateMachineWindow.tool.stateMachineGraphGUI.hoveredStateMachine;
            if (hoveredStateMachine != null)
            {
                //Undo.RecordObjects(new List<UnityEngine.Object> { PAStateMachineWindow.tool.stateMachineGraph.activeStateMachine, hoveredStateMachine }.ToArray(), "Move in StateMachine");
//                PAStateMachineWindow.tool.stateMachineGraph.activeStateMachine.MoveState(this.state, hoveredStateMachine);
                PAStateMachineWindow.tool.RebuildGraph();
            }
        }

        private void MakeTransitionCallback()
        {
            base.graphGUI.edgeGUI.BeginSlotDragging(base.outputSlots.First<Slot>(), true, false);
        }

        public override void NodeUI(UnityEditor.Graphs.GraphGUI host)
        {
            base.graphGUI = host as UnityEditor.Graphs.PAStateMachine.GraphGUI;
            Assert.NotNull(base.graphGUI);
            Event current = Event.current;
			if(UnityEditor.Graphs.PAStateMachine.Node.IsLeftClick())
            {
                host.edgeGUI.EndSlotDragging(base.inputSlots.First<Slot>(), true);
            }
			if(UnityEditor.Graphs.PAStateMachine.Node.IsDoubleClick())
            {
                //Motion motionInternal = this.state.GetMotionInternal(PAStateMachineWindow.tool.motionSetIndex);
                //if (motionInternal is BlendTree)
                {
                    //base.graphGUI.tool.AddBreadCrumb(motionInternal);
                }
                //Selection.activeObject = motionInternal;
                current.Use();
            }
			if(UnityEditor.Graphs.PAStateMachine.Node.IsRightClick())
            {
                GenericMenu menu = new GenericMenu();
                menu.AddItem(new GUIContent("Make Transition"), false, new GenericMenu.MenuFunction(this.MakeTransitionCallback));
                if (base.graphGUI.rootStateMachine.defaultState == this.state)
                {
                    menu.AddDisabledItem(new GUIContent("Set As Default"));
                }
                else
                {
                    menu.AddItem(new GUIContent("Set As Default"), false, new GenericMenu.MenuFunction(this.SetDefaultCallback));
                }
                menu.AddItem(new GUIContent("Copy"), false, new GenericMenu.MenuFunction(this.CopyStateCallback));
                menu.AddItem(new GUIContent("Create new BlendTree in State"), false, new GenericMenu.MenuFunction(this.AddNewBlendTreeCallback));
                menu.AddItem(new GUIContent("Delete"), false, new GenericMenu.MenuFunction(this.DeleteStateCallback));
                menu.ShowAsContext();
                current.Use();
            }
            Rect rect = GUILayoutUtility.GetRect((float) 200f, (float) 10f);
            if ((Event.current.type == EventType.Repaint) && ((base.graphGUI.liveLinkInfo.currentState == this.state) || (base.graphGUI.liveLinkInfo.nextState == this.state)))
            {
                GUIStyle style = "MeLivePlayBackground";
                GUIStyle style2 = "MeLivePlayBar";
                float num = (base.graphGUI.liveLinkInfo.currentState != this.state) ? base.graphGUI.liveLinkInfo.nextStateNormalizedTime : base.graphGUI.liveLinkInfo.currentStateNormalizedTime;
                rect = style.margin.Remove(rect);
                Rect position = style.padding.Remove(rect);
                position.width = (position.width * (num % 1f)) + 2f;
                style2.Draw(position, false, false, false, false);
                style.Draw(rect, false, false, false, false);
            }
        }

        public override void OnDrag()
        {
            base.OnDrag();
            this.state.position = (Vector3) new Vector2(this.position.x, this.position.y);
        }

        private void SetDefaultCallback()
        {
            Undo.RegisterCompleteObjectUndo(base.graphGUI.rootStateMachine, "Set Default State");
            base.graphGUI.rootStateMachine.defaultState = this.state;
            PAStateMachineWindow.tool.RebuildGraph();
        }

        public override UnityEngine.Object selectionObject
        {
            get
            {
                return this.state;
            }
        }

        public override string title
        {
            get
            {
                return this.state.name;
            }
            set
            {
            }
        }

        public override UnityEngine.Object undoableObject
        {
            get
            {
                return this.state;
            }
        }
    }
}

