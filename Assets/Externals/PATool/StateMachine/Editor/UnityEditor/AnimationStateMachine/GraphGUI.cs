﻿namespace UnityEditor.Graphs.PAStateMachine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEditor;
    using UnityEditor.Graphs;
    using UnityEditorInternal;
    using UnityEngine;

	internal class GraphGUI:UnityEditor.Graphs.GraphGUI
    {
        private StateMachineNode m_HoveredStateMachineNode;
        private LiveLinkInfo m_LiveLinkInfo;

        private void AddStateEmptyCallback(object data)
        {
            Undo.RegisterCompleteObjectUndo(this.activeStateMachine, "Empty State Added");
            StateMachineEX.CreateState(this.activeStateMachine,(Vector3) ((Vector2) data));
            PAStateMachineWindow.tool.RebuildGraph();
        }

        private void AddStateFromNewBlendTreeCallback(object data)
        {
            Undo.RegisterCompleteObjectUndo(this.activeStateMachine, "Blend Tree State Added");
			UnityEditorInternal.State state = StateMachineEX.CreateState(this.activeStateMachine,(Vector3)((Vector2)data));
            state.name = "Blend Tree";
            BlendTree tree = state.CreateBlendTree();
            tree.name = "Blend Tree";
            //tree.blendParameter = this.tool.animatorController.GetDefaultBlendTreeParameter();
            //tree.blendParameterY = this.tool.animatorController.GetDefaultBlendTreeParameter();
            PAStateMachineWindow.tool.RebuildGraph();
        }

        private void AddStateFromSelectedMotionCallback(object data)
        {
            Undo.RegisterCompleteObjectUndo(this.activeStateMachine, "Clip State Added");
            AnimationClip activeObject = Selection.activeObject as AnimationClip;
			UnityEditorInternal.State state = StateMachineEX.CreateState(this.activeStateMachine,(Vector3)((Vector2)data));
            state.name = activeObject.name;
            state.SetAnimationClip(activeObject);
            PAStateMachineWindow.tool.RebuildGraph();
        }

        private void AddStateMachineCallback(object data)
        {
            Undo.RegisterCompleteObjectUndo(this.activeStateMachine, "Sub-State Machine Added");
            this.activeStateMachine.AddStateMachine("New StateMachine");
            this.activeStateMachine.SetStateMachinePosition((int) (this.activeStateMachine.stateMachineCount - 1), (Vector3) ((Vector2) data));
            PAStateMachineWindow.tool.RebuildGraph();
        }

        public void ClearSelection()
        {
            base.selection.Clear();
            this.edgeGUI.edgeSelection.Clear();
            this.UpdateUnitySelection();
        }

        private List<string> CollectSelectionNames()
        {
            List<string> list = new List<string>();
			foreach(UnityEditor.Graphs.PAStateMachine.Node node in base.selection)
            {
                if (node is StateNode)
                {
                    list.Add((node as StateNode).state.name);
                }
                else if (node is StateMachineNode)
                {
                    StateMachine stateMachine = (node as StateMachineNode).stateMachine;
                    if (this.parentStateMachine != stateMachine)
                    {
                        list.Add(stateMachine.name);
                    }
                }
            }
            foreach (int num in this.edgeGUI.edgeSelection)
            {
                foreach (Transition transition in this.stateMachineGraph.GetEdgeInfo(base.graph.edges[num]).transitions)
                {
                    list.Add(transition.uniqueName);
                }
            }
            return list;
        }

        private void CopyStateMachineCallback(object data)
        {
            Unsupported.CopyStateMachineToPasteboard(this.activeStateMachine, PAStateMachineWindow.tool.animatorController);
        }

        protected bool DeleteEvent()
        {
            Event current = Event.current;
            if ((current.type != EventType.KeyDown) && (current.keyCode == KeyCode.Delete))
            {
                return true;
            }
            if ((current.commandName != "SoftDelete") && (current.commandName != "Delete"))
            {
                return false;
            }
            if (current.type == EventType.ValidateCommand)
            {
                current.Use();
                return false;
            }
            return (current.type == EventType.ExecuteCommand);
        }

        public static bool DeleteNodeDialog(string[] toDelete)
        {
            string title = "Delete selected asset";
            if (toDelete.Length > 1)
            {
                title = title + "s";
            }
            title = title + "?";
            string message = string.Empty;
            foreach (string str3 in toDelete)
            {
                message = message + str3 + "\n";
            }
            message = message + "\nYou cannot undo this action.";
            return EditorUtility.DisplayDialog(title, message, "Delete", "Cancel");
        }

        private void DeleteSelectedEdges()
        {
            List<Edge> list = new List<Edge>();
            foreach (int num in this.edgeGUI.edgeSelection)
            {
                list.Add(base.graph.edges[num]);
            }
            foreach (Edge edge in list)
            {
                foreach (Transition transition in this.stateMachineGraph.GetEdgeInfo(edge).transitions)
                {
                    this.rootStateMachine.RemoveTransition(transition);
                }
                this.stateMachineGraph.RemoveEdge(edge);
            }
            this.edgeGUI.edgeSelection.Clear();
        }

        private void DeleteSelectedNodes()
        {
#if false
			foreach(UnityEditor.Graphs.PAStateMachine.Node node in base.selection)
            {
                if (node is StateNode)
                {
                    UnityEditorInternal.State state = (node as StateNode).state;
                    BlendTree motionInternal = state.GetMotionInternal(PAStateMachineWindow.tool.motionSetIndex) as BlendTree;
                    if ((motionInternal != null) && MecanimUtilities.AreSameAsset(motionInternal, this.activeStateMachine))
                    {
                        MecanimUtilities.DestroyBlendTreeRecursive(motionInternal);
                    }
                    this.rootStateMachine.RemoveState(state);
                    this.stateMachineGraph.RemoveNode(node);
                }
                if (node is StateMachineNode)
                {
                    StateMachine stateMachine = (node as StateMachineNode).stateMachine;
                    if (this.parentStateMachine != stateMachine)
                    {
                        this.rootStateMachine.RemoveStateMachine(stateMachine);
                        this.stateMachineGraph.RemoveNode(node);
                    }
                }
            }
#endif
        }

        public void DeleteSelection()
        {
            List<string> list = this.CollectSelectionNames();
            if (list.Count != 0)
            {
                if (DeleteNodeDialog(list.ToArray()))
                {
                    this.DeleteSelectedEdges();
                    this.DeleteSelectedNodes();
                }
                base.selection.Clear();
                this.UpdateUnitySelection();
            }
        }

        private void HandleContextMenu()
        {
            if (Event.current.type == EventType.ContextClick)
            {
                GenericMenu menu = new GenericMenu();
                menu.AddItem(new GUIContent("Create State/Empty"), false, new GenericMenu.MenuFunction2(this.AddStateEmptyCallback), Event.current.mousePosition);
                if (HasMotionSelected())
                {
                    menu.AddItem(new GUIContent("Create State/From Selected Clip"), false, new GenericMenu.MenuFunction2(this.AddStateFromSelectedMotionCallback), Event.current.mousePosition);
                }
                else
                {
                    menu.AddDisabledItem(new GUIContent("Create State/From Selected Clip"));
                }
                menu.AddItem(new GUIContent("Create State/From New Blend Tree"), false, new GenericMenu.MenuFunction2(this.AddStateFromNewBlendTreeCallback), Event.current.mousePosition);
                menu.AddItem(new GUIContent("Create Sub-State Machine"), false, new GenericMenu.MenuFunction2(this.AddStateMachineCallback), Event.current.mousePosition);
                if (Unsupported.HasStateMachineDataInPasteboard())
                {
                    menu.AddItem(new GUIContent("Paste"), false, new GenericMenu.MenuFunction2(this.PasteCallback), Event.current.mousePosition);
                }
                else
                {
                    menu.AddDisabledItem(new GUIContent("Paste"));
                }
                menu.AddItem(new GUIContent("Copy current StateMachine"), false, new GenericMenu.MenuFunction2(this.CopyStateMachineCallback), Event.current.mousePosition);
                menu.ShowAsContext();
            }
        }

        private void HandleDeleteEvent()
        {
            if (this.DeleteEvent())
            {
                this.DeleteSelection();
            }
        }

        private void HandleObjectDragging()
        {
            Event current = Event.current;
            List<AnimationClip> draggedMotions = DragAndDrop.objectReferences.OfType<AnimationClip>().ToList<AnimationClip>();
            switch (current.type)
            {
                case EventType.Repaint:
                    if ((this.isSelectionMoving && (this.m_HoveredStateMachineNode != null)) && !base.selection.Contains(this.m_HoveredStateMachineNode))
                    {
                        EditorGUIUtility.AddCursorRect(this.m_HoveredStateMachineNode.position, MouseCursor.ArrowPlus);
                    }
                    return;

                case EventType.DragUpdated:
                case EventType.DragPerform:
                    if (!this.ValideDraggedMotion(ref draggedMotions, current.type == EventType.DragPerform))
                    {
                        DragAndDrop.visualMode = DragAndDropVisualMode.None;
                    }
                    else
                    {
                        DragAndDrop.visualMode = DragAndDropVisualMode.Generic;
                    }
                    if ((current.type == EventType.DragPerform) && (DragAndDrop.visualMode != DragAndDropVisualMode.None))
                    {
                        DragAndDrop.AcceptDrag();
                        Undo.RegisterCompleteObjectUndo(this.activeStateMachine, "Drag motion to state machine.");
                        for (int i = 0; i < draggedMotions.Count; i++)
                        {
                            AnimationClip clip = draggedMotions[i];
							UnityEditorInternal.State state = StateMachineEX.CreateState(this.activeStateMachine,(Vector3)(current.mousePosition + (new Vector2(12f,12f) * i)));
                            state.SetAnimationClip(clip);
                            state.name = clip.name;
                        }
                        this.stateMachineGraph.RebuildGraph();
                    }
                    current.Use();
                    return;

                case EventType.DragExited:
                    this.ValideDraggedMotion(ref draggedMotions, true);
                    current.Use();
                    return;
            }
        }

        private static bool HasMotionSelected()
        {
            return (Selection.activeObject is Motion);
        }

		private bool IsCurrentStateMachineNodeLiveLinked(UnityEditor.Graphs.PAStateMachine.Node n)
        {
            StateMachineNode node = n as StateMachineNode;
            if (node != null)
            {
                UnityEditorInternal.State currentState = this.liveLinkInfo.currentState;
				bool flag = StateMachineEX.HasState(this.activeStateMachine,currentState,true);
				bool flag2 = StateMachineEX.HasState(node.stateMachine,currentState,true);
				bool flag3 = StateMachineEX.HasStateMachine(node.stateMachine,this.activeStateMachine,false);
                if (((flag3 && flag2) && !flag) || (!flag3 && flag2))
                {
                    return true;
                }
            }
            return false;
        }

        private void LiveLink()
        {
#if false
            this.m_LiveLinkInfo.Clear();
            if (this.tool.liveLink)
            {
                Animator previewObject = this.tool.previewObject;
                AnimatorStateInfo currentAnimatorStateInfo = previewObject.GetCurrentAnimatorStateInfo(PAStateMachineWindow.tool.selectedLayerIndex);
                AnimatorStateInfo nextAnimatorStateInfo = previewObject.GetNextAnimatorStateInfo(PAStateMachineWindow.tool.selectedLayerIndex);
                AnimatorTransitionInfo animatorTransitionInfo = previewObject.GetAnimatorTransitionInfo(PAStateMachineWindow.tool.selectedLayerIndex);
                int nameHash = currentAnimatorStateInfo.nameHash;
                int stateUniqueNameHash = nextAnimatorStateInfo.nameHash;
                this.m_LiveLinkInfo.currentState = (nameHash == 0) ? null : this.rootStateMachine.FindState(nameHash);
                this.m_LiveLinkInfo.currentStateNormalizedTime = currentAnimatorStateInfo.normalizedTime;
                this.m_LiveLinkInfo.nextState = (stateUniqueNameHash == 0) ? null : this.rootStateMachine.FindState(stateUniqueNameHash);
                this.m_LiveLinkInfo.nextStateNormalizedTime = nextAnimatorStateInfo.normalizedTime;
                int transitionUniqueName = animatorTransitionInfo.nameHash;
                this.m_LiveLinkInfo.currentTransition = this.rootStateMachine.FindTransition(transitionUniqueName);
                this.m_LiveLinkInfo.currentTransitionProgress = animatorTransitionInfo.normalizedTime;
                bool flag = (transitionUniqueName != 0) && (this.m_LiveLinkInfo.currentTransition == null);
                if ((this.tool.autoLiveLink && (this.m_LiveLinkInfo.currentState != null)) && (this.m_LiveLinkInfo.nextState != null))
                {
                    StateMachine machine3;
                    StateMachine stateMachine = this.m_LiveLinkInfo.currentState.stateMachine;
                    StateMachine machine2 = (this.m_LiveLinkInfo.nextState == null) ? null : this.m_LiveLinkInfo.nextState.stateMachine;
                    if ((this.m_LiveLinkInfo.currentTransitionProgress < 0.5) && !flag)
                    {
                        machine3 = stateMachine;
                    }
                    else
                    {
                        machine3 = machine2;
                    }
                    if ((machine3 != this.activeStateMachine) && (Event.current.type == EventType.Repaint))
                    {
                        List<StateMachine> hierarchy = new List<StateMachine>();
                        MecanimUtilities.StateMachineRelativePath(this.rootStateMachine, machine3, ref hierarchy);
                        this.tool.BuildBreadCrumbsFromSMHierarchy(hierarchy);
                    }
                }
            }
#endif
        }

        private bool MoveSelectionTo(StateMachineNode targetSM)
        {
            bool flag = false;
            List<UnityEngine.Object> list = new List<UnityEngine.Object> {
                this.rootStateMachine
            };
			list.AddRange(StateMachineEX.stateMachinesRecursive(this.rootStateMachine).ToArray());
            foreach (UnityEditor.Graphs.PAStateMachine.Node node in base.selection)
            {
                if (node is StateNode)
                {
                    list.Add((node as StateNode).state);
                }
            }
            Undo.RegisterCompleteObjectUndo(list.ToArray(), "Move in StateMachine");
			foreach(UnityEditor.Graphs.PAStateMachine.Node node2 in base.selection)
            {
                if (node2 is StateNode)
                {
                    //this.rootStateMachine.MoveState((node2 as StateNode).state, targetSM.stateMachine);
                }
                else if (node2 is StateMachineNode)
                {
                    //this.rootStateMachine.MoveStateMachine((node2 as StateMachineNode).stateMachine, targetSM.stateMachine);
                }
                flag = true;
            }
            return flag;
        }

        public override void NodeGUI(UnityEditor.Graphs.Node n)
        {
            GUILayoutUtility.GetRect((float) 160f, (float) 0f);
            base.SelectNode(n);
            n.NodeUI(this);
            base.DragNodes();
        }

        public override void OnGraphGUI()
        {
            if (this.stateMachineGraph.DisplayDirty())
            {
                this.stateMachineGraph.RebuildGraph();
            }
            this.SyncGraphToUnitySelection();
            this.LiveLink();
            this.SetHoveredStateMachine();
            base.m_Host.BeginWindows();
			foreach(UnityEditor.Graphs.PAStateMachine.Node node in base.m_Graph.nodes)
            {
                c__AnonStorey15 storey = new c__AnonStorey15 {
                    f__this = this,
                    n2 = node
                };
                bool on = base.selection.Contains(node);
                GUILayoutOption[] options = new GUILayoutOption[] { GUILayout.Width(0f), GUILayout.Height(0f) };
                node.position = GUILayout.Window(node.GetInstanceID(), node.position, new GUI.WindowFunction(storey.m__33), node.title, UnityEditor.Graphs.Styles.GetNodeStyle(node.style, !this.IsCurrentStateMachineNodeLiveLinked(node) ? node.color : UnityEditor.Graphs.Styles.Color.Blue, on), options);
                if ((Event.current.type == EventType.MouseMove) && node.position.Contains(Event.current.mousePosition))
                {
                    this.edgeGUI.SlotDragging(node.inputSlots.First<Slot>(), true, true);
                }
            }
            this.edgeGUI.DoEdges();
            base.m_Host.EndWindows();
            if ((Event.current.type == EventType.MouseDown) && (Event.current.button != 2))
            {
                this.edgeGUI.EndDragging();
            }
            this.HandleDeleteEvent();
            this.HandleContextMenu();
            this.HandleObjectDragging();
            base.DragSelection(new Rect(-5000f, -5000f, 10000f, 10000f));
            this.stateMachineGraph.ReadNodePositions();
        }

        private void PasteCallback(object data)
        {
            Undo.RegisterCompleteObjectUndo(this.activeStateMachine, "Paste");
            int stateCount = this.activeStateMachine.stateCount;
            int stateMachineCount = this.activeStateMachine.stateMachineCount;
            Unsupported.PasteToStateMachineFromPasteboard(this.activeStateMachine, this.tool.animatorController);
            if (stateCount == (this.activeStateMachine.stateCount - 1))
            {
                this.activeStateMachine.GetState(this.activeStateMachine.stateCount - 1).position = (Vector3) ((Vector2) data);
            }
            else if (stateMachineCount == (this.activeStateMachine.stateMachineCount - 1))
            {
                this.activeStateMachine.SetStateMachinePosition(stateMachineCount, (Vector3) ((Vector2) data));
            }
            PAStateMachineWindow.tool.RebuildGraph();
        }

        private void SetHoveredStateMachine()
        {
            Vector2 mousePosition = Event.current.mousePosition;
            this.m_HoveredStateMachineNode = null;
			foreach(UnityEditor.Graphs.PAStateMachine.Node node in base.m_Graph.nodes)
            {
                StateMachineNode item = node as StateMachineNode;
                if (((item != null) && item.position.Contains(mousePosition)) && !base.selection.Contains(item))
                {
                    this.m_HoveredStateMachineNode = item;
                    break;
                }
            }
        }

        private void SyncGraphToUnitySelection()
        {
            if (GUIUtility.hotControl == 0)
            {
                base.selection.Clear();
                foreach (UnityEngine.Object obj2 in Selection.objects)
                {
					UnityEditor.Graphs.PAStateMachine.Node item = null;
                    UnityEditorInternal.State state = obj2 as UnityEditorInternal.State;
                    if (state != null)
                    {
                        item = this.stateMachineGraph.FindNode(state);
                    }
                    StateMachine stateMachine = obj2 as StateMachine;
                    if (stateMachine != null)
                    {
                        item = this.stateMachineGraph.FindNode(stateMachine);
                    }
                    AnyStateNode node2 = obj2 as AnyStateNode;
                    if (node2 != null)
                    {
                        item = this.stateMachineGraph.FindNode((UnityEditorInternal.State) null);
                    }
                    if (item != null)
                    {
                        base.selection.Add(item);
                    }
                }
            }
        }

        protected override void UpdateUnitySelection()
        {
            List<UnityEngine.Object> list = new List<UnityEngine.Object>();
            foreach (UnityEditor.Graphs.Node node in base.selection)
            {
                if (node is StateNode)
                {
                    list.Add((node as StateNode).state);
                }
                if (node is StateMachineNode)
                {
                    list.Add((node as StateMachineNode).stateMachine);
                }
                if (node is AnyStateNode)
                {
                    list.Add(node);
                }
            }
            if (list.Count == 0)
            {
                foreach (int num in this.edgeGUI.edgeSelection)
                {
                    EdgeInfo edgeInfo = this.stateMachineGraph.GetEdgeInfo(base.graph.edges[num]);
                    list.AddRange(edgeInfo.transitions.OfType<UnityEngine.Object>());
                }
            }
            Selection.objects = list.ToArray();
        }

        private bool ValideDraggedMotion(ref List<AnimationClip> draggedMotions, bool showWarning)
        {
            for (int i = 0; i < draggedMotions.Count; i++)
            {
                AnimationClip item = draggedMotions[i];
                if (!item.ValidateIfRetargetable(showWarning))
                {
                    draggedMotions.Remove(item);
                }
            }
            return (draggedMotions.Count > 0);
        }

        public StateMachine activeStateMachine
        {
            get
            {
                return this.stateMachineGraph.activeStateMachine;
            }
        }

        public override IEdgeGUI edgeGUI
        {
            get
            {
                if (base.m_EdgeGUI == null)
                {
					UnityEditor.Graphs.PAStateMachine.EdgeGUI egui = new UnityEditor.Graphs.PAStateMachine.EdgeGUI
					{
                        host = this
                    };
                    base.m_EdgeGUI = egui;
                }
                return base.m_EdgeGUI;
            }
        }

        public StateMachine hoveredStateMachine
        {
            get
            {
                return ((this.m_HoveredStateMachineNode == null) ? null : this.m_HoveredStateMachineNode.stateMachine);
            }
        }

        private bool isSelectionMoving
        {
            get
            {
                return ((base.selection.Count > 0) && base.selection[0].isDragging);
            }
        }

        public LiveLinkInfo liveLinkInfo
        {
            get
            {
                return this.m_LiveLinkInfo;
            }
        }

        public StateMachine parentStateMachine
        {
            get
            {
                return this.stateMachineGraph.parentStateMachine;
            }
        }

        public StateMachine rootStateMachine
        {
            get
            {
                return this.stateMachineGraph.rootStateMachine;
            }
        }

		public UnityEditor.Graphs.PAStateMachine.Graph stateMachineGraph
        {
            get
            {
				return (base.graph as UnityEditor.Graphs.PAStateMachine.Graph);
            }
        }

        public PAStateMachineWindow tool
        {
            get
            {
                return (base.m_Host as PAStateMachineWindow);
            }
        }

        [CompilerGenerated]
        private sealed class c__AnonStorey15
        {
			internal UnityEditor.Graphs.PAStateMachine.GraphGUI f__this;
			internal UnityEditor.Graphs.PAStateMachine.Node n2;

            internal void m__33(int id)
            {
                this.f__this.NodeGUI(this.n2);
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct LiveLinkInfo
        {
            private UnityEditorInternal.State m_CurrentState;
            private float m_CurrentStateNormalizedTime;
            private UnityEditorInternal.State m_NextState;
            private float m_NextStateNormalizedTime;
            private Transition m_CurrentTransition;
            private float m_CurrentTransitionProgress;
            public UnityEditorInternal.State currentState
            {
                get
                {
                    return this.m_CurrentState;
                }
                set
                {
                    this.m_CurrentState = value;
                }
            }
            public float currentStateNormalizedTime
            {
                get
                {
                    return this.m_CurrentStateNormalizedTime;
                }
                set
                {
                    this.m_CurrentStateNormalizedTime = value;
                }
            }
            public UnityEditorInternal.State nextState
            {
                get
                {
                    return this.m_NextState;
                }
                set
                {
                    this.m_NextState = value;
                }
            }
            public float nextStateNormalizedTime
            {
                get
                {
                    return this.m_NextStateNormalizedTime;
                }
                set
                {
                    this.m_NextStateNormalizedTime = value;
                }
            }
            public Transition currentTransition
            {
                get
                {
                    return this.m_CurrentTransition;
                }
                set
                {
                    this.m_CurrentTransition = value;
                }
            }
            public float currentTransitionProgress
            {
                get
                {
                    return this.m_CurrentTransitionProgress;
                }
                set
                {
                    this.m_CurrentTransitionProgress = value;
                }
            }
            public void Clear()
            {
                this.m_CurrentState = null;
                this.m_NextState = null;
                this.m_CurrentStateNormalizedTime = 0f;
                this.m_NextStateNormalizedTime = 0f;
                this.m_CurrentTransition = null;
                this.m_CurrentTransitionProgress = 0f;
            }
        }
    }
}

