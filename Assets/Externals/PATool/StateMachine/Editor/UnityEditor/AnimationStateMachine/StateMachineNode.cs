﻿namespace UnityEditor.Graphs.PAStateMachine
{
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEditor;
    using UnityEditor.Graphs;
    using UnityEditorInternal;
    using UnityEngine;

	class StateMachineNode:UnityEditor.Graphs.PAStateMachine.Node
    {
        public StateMachine stateMachine;

        private void CopyStateMachineCallback()
        {
            Unsupported.CopyStateMachineToPasteboard(this.stateMachine, PAStateMachineWindow.tool.animatorController);
        }

        public void DeleteStateMachineCallback()
        {
            if (!base.graphGUI.selection.Contains(this))
            {
                base.graphGUI.selection.Add(this);
            }
            base.graphGUI.DeleteSelection();
            PAStateMachineWindow.tool.RebuildGraph();
        }

        public override void EndDrag()
        {
            base.EndDrag();
            StateMachine hoveredStateMachine = PAStateMachineWindow.tool.stateMachineGraphGUI.hoveredStateMachine;
            if ((hoveredStateMachine != null) && (hoveredStateMachine != this.stateMachine))
            {
#if false
                Undo.RecordObjects(new List<UnityEngine.Object> { PAStateMachineWindow.tool.stateMachineGraph.activeStateMachine, hoveredStateMachine }.ToArray(), "Move in StateMachine");
                PAStateMachineWindow.tool.stateMachineGraph.activeStateMachine.MoveStateMachine(this.stateMachine, hoveredStateMachine);
#endif
                PAStateMachineWindow.tool.RebuildGraph();
            }
        }

        public override void NodeUI(UnityEditor.Graphs.GraphGUI host)
        {
			base.graphGUI = host as UnityEditor.Graphs.PAStateMachine.GraphGUI;
            Assert.NotNull(base.graphGUI);
			if(UnityEditor.Graphs.PAStateMachine.Node.IsLeftClick())
            {
                host.edgeGUI.EndSlotDragging(base.inputSlots.First<Slot>(), true);
            }
			if(UnityEditor.Graphs.PAStateMachine.Node.IsDoubleClick())
            {
                if (this.stateMachine == base.graphGUI.stateMachineGraph.parentStateMachine)
                {
                    base.graphGUI.tool.GoToBreadCrumbTarget(this.stateMachine);
                }
                else
                {
                    base.graphGUI.tool.AddBreadCrumb(this.stateMachine);
                }
                Event.current.Use();
            }
			if(UnityEditor.Graphs.PAStateMachine.Node.IsRightClick())
            {
                GenericMenu menu = new GenericMenu();
                menu.AddItem(new GUIContent("Copy"), false, new GenericMenu.MenuFunction(this.CopyStateMachineCallback));
                menu.AddItem(new GUIContent("Delete"), false, new GenericMenu.MenuFunction(this.DeleteStateMachineCallback));
                menu.ShowAsContext();
                Event.current.Use();
            }
        }

        public override void OnDrag()
        {
            base.OnDrag();
            if (this.stateMachine == PAStateMachineWindow.tool.stateMachineGraph.parentStateMachine)
            {
                PAStateMachineWindow.tool.stateMachineGraph.activeStateMachine.parentStateMachinePosition = (Vector3) new Vector2(this.position.x, this.position.y);
            }
            else
            {
				StateMachineEX.SetStateMachinePosition(PAStateMachineWindow.tool.stateMachineGraph.activeStateMachine,this.stateMachine,(Vector3)new Vector2(this.position.x,this.position.y));
            }
        }

        public override UnityEngine.Object selectionObject
        {
            get
            {
                return this.stateMachine;
            }
        }

        public override string title
        {
            get
            {
                return (base.title + this.stateMachine.name);
            }
            set
            {
                base.title = value;
            }
        }

        public override UnityEngine.Object undoableObject
        {
            get
            {
                return PAStateMachineWindow.tool.stateMachineGraph.activeStateMachine;
            }
        }
    }
}

