﻿namespace UnityEditor.Graphs.PAStateMachine
{
    using NUnit.Framework;
    using System;
	using UnityEditorInternal;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEditor.Graphs;
    using UnityEngine;

	using UnityEditor.Graphs.AnimationStateMachine;

	internal class Graph:UnityEditor.Graphs.Graph
    {
        [NonSerialized]
        private StateMachine m_ActiveStateMachine;
        [NonSerialized]
        private AnyStateNode m_AnyStateNode;
        private readonly Dictionary<string, EdgeInfo> m_ConnectedSlotsCache = new Dictionary<string, EdgeInfo>();
        private UnityEditorInternal.State m_DefaultState;
        private int m_StateCount;
        private int m_StateMachineCount;
        private readonly Dictionary<StateMachine, StateMachineNode> m_StateMachineNodeLookup = new Dictionary<StateMachine, StateMachineNode>();
        private readonly Dictionary<UnityEditorInternal.State, StateNode> m_StateNodeLookup = new Dictionary<UnityEditorInternal.State, StateNode>();
        private int m_TransitionCount;
        [NonSerialized]
        public StateMachine parentStateMachine;
        [NonSerialized]
        public StateMachine rootStateMachine;

        public void BuildGraphFromStateMachine(StateMachine stateMachine)
        {
            Assert.IsNotNull(stateMachine);
            Assert.IsNotNull(this.rootStateMachine);
            this.Clear();
            this.m_ActiveStateMachine = stateMachine;
            this.CreateNodes();
            this.CreateEdges();
            this.m_StateCount = this.m_ActiveStateMachine.stateCount;
            this.m_StateMachineCount = this.m_ActiveStateMachine.stateMachineCount;
            this.m_TransitionCount = StateMachineEX.transitionCount(this.rootStateMachine);
            this.m_DefaultState = this.rootStateMachine.defaultState;
        }

		private T CreateAndAddNode<T>(string name,Vector3 position) where T:UnityEditor.Graphs.PAStateMachine.Node
        {
            T node = ScriptableObject.CreateInstance<T>();
            node.hideFlags = HideFlags.HideAndDontSave;
            node.name = name;
            node.position = new Rect(position.x, position.y, 0f, 0f);
            node.AddInputSlot("In");
            node.AddOutputSlot("Out");
            this.AddNode(node);
            return node;
        }

        private void CreateAnyStateNode()
        {
            this.m_AnyStateNode = this.CreateAndAddNode<AnyStateNode>("Any State", this.activeStateMachine.anyStatePosition);
            this.m_AnyStateNode.color = UnityEditor.Graphs.Styles.Color.Aqua;
        }

        private void CreateEdges()
        {
            this.m_ConnectedSlotsCache.Clear();
			foreach(Transition transition in StateMachineEX.transitions(this.rootStateMachine))
            {
                UnityEditorInternal.State srcState = transition.srcState;
                UnityEditorInternal.State dstState = transition.dstState;
                if ((srcState != null) || (dstState.stateMachine == this.activeStateMachine))
                {
					UnityEditor.Graphs.PAStateMachine.Node srcNode = this.FindNode(srcState);
					UnityEditor.Graphs.PAStateMachine.Node dstNode = this.FindNode(dstState);
                    string key = GenerateConnectionKey(srcNode, dstNode);
                    if (this.m_ConnectedSlotsCache.ContainsKey(key))
                    {
                        this.m_ConnectedSlotsCache[key].transitions.Add(transition);
                    }
                    else if (srcNode != dstNode)
                    {
                        Slot fromSlot = srcNode.outputSlots.First<Slot>();
                        Slot toSlot = dstNode.inputSlots.First<Slot>();
                        this.Connect(fromSlot, toSlot);
                        this.m_ConnectedSlotsCache.Add(key, new EdgeInfo(transition));
                    }
                }
            }
        }

        private void CreateNodeFromState(UnityEditorInternal.State state)
        {
            StateNode node = this.CreateAndAddNode<StateNode>(string.Empty, state.position);
            node.state = state;
            if (this.rootStateMachine.defaultState == state)
            {
                node.color = UnityEditor.Graphs.Styles.Color.Orange;
            }
            this.m_StateNodeLookup.Add(state, node);
        }

        private void CreateNodeFromStateMachine(StateMachine subStateMachine)
        {
            StateMachineNode node = this.CreateAndAddNode<StateMachineNode>(string.Empty, StateMachineEX.GetStateMachinePosition(this.activeStateMachine,subStateMachine));
            node.stateMachine = subStateMachine;
            node.style = "node hex";
			if(StateMachineEX.HasState(subStateMachine,this.rootStateMachine.defaultState))
            {
                node.color = UnityEditor.Graphs.Styles.Color.Orange;
            }
            this.m_StateMachineNodeLookup.Add(subStateMachine, node);
        }

        private void CreateNodes()
        {
            this.m_StateNodeLookup.Clear();
            this.m_StateMachineNodeLookup.Clear();
			IEnumerator<UnityEditorInternal.State> enumerator = StateMachineEX.states(this.activeStateMachine).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    UnityEditorInternal.State current = enumerator.Current;
                    this.CreateNodeFromState(current);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
			IEnumerator<StateMachine> enumerator2 = StateMachineEX.stateMachines(this.activeStateMachine).GetEnumerator();
            try
            {
                while (enumerator2.MoveNext())
                {
                    StateMachine subStateMachine = enumerator2.Current;
                    this.CreateNodeFromStateMachine(subStateMachine);
                }
            }
            finally
            {
                if (enumerator2 == null)
                {
                }
                enumerator2.Dispose();
            }
            this.CreateAnyStateNode();
            if (this.parentStateMachine != null)
            {
                this.CreateParentStateMachineNode();
            }
        }

        private void CreateParentStateMachineNode()
        {
            StateMachineNode node = this.CreateAndAddNode<StateMachineNode>("(Up) ", this.activeStateMachine.parentStateMachinePosition);
            node.stateMachine = this.parentStateMachine;
            node.style = "node hex";
			if(StateMachineEX.HasState(this.parentStateMachine,this.rootStateMachine.defaultState) && !StateMachineEX.HasState(this.activeStateMachine,this.rootStateMachine.defaultState))
            {
                node.color = UnityEditor.Graphs.Styles.Color.Orange;
            }
            this.m_StateMachineNodeLookup.Add(this.parentStateMachine, node);
        }

        public bool DisplayDirty()
        {
			return ((this.activeStateMachine.stateCount != this.m_StateCount) || ((this.activeStateMachine.stateMachineCount != this.m_StateMachineCount) || ((StateMachineEX.transitionCount(this.rootStateMachine) != this.m_TransitionCount) || (this.rootStateMachine.defaultState != this.m_DefaultState))));
        }

		public UnityEditor.Graphs.PAStateMachine.Node FindNode(UnityEditorInternal.State state)
        {
            if (state == null)
            {
                return this.m_AnyStateNode;
            }
            if (this.m_StateNodeLookup.ContainsKey(state))
            {
                return this.m_StateNodeLookup[state];
            }
			IEnumerator<StateMachine> enumerator = StateMachineEX.stateMachines(this.activeStateMachine).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    StateMachine current = enumerator.Current;
					if(StateMachineEX.HasState(current,state))
                    {
                        return this.m_StateMachineNodeLookup[current];
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            if (this.parentStateMachine != null)
            {
                return this.m_StateMachineNodeLookup[this.parentStateMachine];
            }
            return null;
        }

		public UnityEditor.Graphs.PAStateMachine.Node FindNode(StateMachine stateMachine)
        {
            if (stateMachine != null)
            {
                if (stateMachine == this.activeStateMachine)
                {
                    return null;
                }
                if (this.m_StateMachineNodeLookup.ContainsKey(stateMachine))
                {
                    return this.m_StateMachineNodeLookup[stateMachine];
                }
				IEnumerator<StateMachine> enumerator = StateMachineEX.stateMachines(this.activeStateMachine).GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        StateMachine current = enumerator.Current;
                        if (StateMachineEX.HasStateMachine(current,stateMachine))
                        {
                            return this.m_StateMachineNodeLookup[current];
                        }
                    }
                }
                finally
                {
                    if (enumerator == null)
                    {
                    }
                    enumerator.Dispose();
                }
                if (this.parentStateMachine != null)
                {
                    return this.m_StateMachineNodeLookup[this.parentStateMachine];
                }
            }
            return null;
        }

        private static string GenerateConnectionKey(UnityEditor.Graphs.Node srcNode, UnityEditor.Graphs.Node dstNode)
        {
            return (srcNode.GetInstanceID() + "->" + dstNode.GetInstanceID());
        }

        public EdgeInfo GetEdgeInfo(Edge edge)
        {
            if (edge.toSlot == null)
            {
                return null;
            }
            return this.m_ConnectedSlotsCache[GenerateConnectionKey(edge.fromSlot.node, edge.toSlot.node)];
        }

        internal UnityEditor.Graphs.GraphGUI GetEditor()
        {
			UnityEditor.Graphs.PAStateMachine.GraphGUI hgui = ScriptableObject.CreateInstance<UnityEditor.Graphs.PAStateMachine.GraphGUI>();
            hgui.graph = this;
            hgui.hideFlags = HideFlags.HideAndDontSave;
            return hgui;
        }

        private void ReadAnyStatePosition(AnyStateNode anyStateNode)
        {
            anyStateNode.position.x = this.activeStateMachine.anyStatePosition.x;
            anyStateNode.position.y = this.activeStateMachine.anyStatePosition.y;
        }

        public void ReadNodePositions()
        {
			foreach(UnityEditor.Graphs.PAStateMachine.Node node in base.nodes)
            {
                if (node is StateNode)
                {
                    ReadStatePosition(node as StateNode);
                }
                if (node is StateMachineNode)
                {
                    this.ReadStateMachinePosition(node as StateMachineNode);
                }
                if (node is AnyStateNode)
                {
                    this.ReadAnyStatePosition(node as AnyStateNode);
                }
            }
        }

        private void ReadStateMachinePosition(StateMachineNode stateMachineNode)
        {
            Vector2 parentStateMachinePosition;
            if (stateMachineNode.stateMachine == this.parentStateMachine)
            {
                parentStateMachinePosition = this.activeStateMachine.parentStateMachinePosition;
            }
            else
            {
				parentStateMachinePosition = StateMachineEX.GetStateMachinePosition(this.activeStateMachine,stateMachineNode.stateMachine);
            }
            stateMachineNode.position.x = parentStateMachinePosition.x;
            stateMachineNode.position.y = parentStateMachinePosition.y;
        }

        private static void ReadStatePosition(StateNode stateNode)
        {
            Vector2 position = stateNode.state.position;
            stateNode.position.x = position.x;
            stateNode.position.y = position.y;
        }

        public void RebuildGraph()
        {
            this.BuildGraphFromStateMachine(this.activeStateMachine);
        }

        public StateMachine activeStateMachine
        {
            get
            {
                return this.m_ActiveStateMachine;
            }
            set
            {
                if (this.m_ActiveStateMachine != value)
                {
                    this.BuildGraphFromStateMachine(value);
                }
            }
        }
    }
}

