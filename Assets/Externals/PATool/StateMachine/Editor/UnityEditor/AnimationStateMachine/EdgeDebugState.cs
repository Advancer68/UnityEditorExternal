﻿namespace UnityEditor.Graphs.PAStateMachine
{
    using System;

    internal enum EdgeDebugState
    {
        Normal,
        SoloSome,
        SoloAll,
        MuteSome,
        MuteAll,
        MuteAndSolo
    }
}

