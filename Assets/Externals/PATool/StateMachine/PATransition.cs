﻿using UnityEngine;
using System.Collections;

namespace PAExternal.StateMachine
{

	public class PATransition:PAObject
	{
		public int tag;
		public PAState srcState;
		public PAState drcState;
		public PACondition condition;
		// Use this for initialization
		void Start()
		{

		}

		// Update is called once per frame
		void Update()
		{

		}
	}
}
