﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using System.Diagnostics;
using System;

public class CobjectPool<T> where T:new()
{
	private Callback autocall = null;
	/// <summary>
	/// 自动回收对象的已间隔时间 当大于cullInterval时
	/// alapse -= cullInterval
	/// </summary>
	private float elapse = 0;
	/// <summary>
	/// log的前缀
	/// </summary>
	private string logPrefix = "";
	/// <summary>
	/// 已生成使用中的对象
	/// </summary>
	private List<T> _spawned = new List<T>();
	/// <summary>
	/// 已生成未使用的对象
	/// </summary>
	private Queue<T> _dispawn = new Queue<T>();
	/// <summary>
	/// 预加载的对象数量
	/// </summary>
	private int preLoadAmount
	{
		get;
		set;
	}
	/// <summary>
	/// 常用对象池对象的保有量
	/// 超过后剔除
	/// </summary>
	public int cullAbove
	{
		get;
		set;
	}
	/// <summary>
	/// 超过常用数量的对象回收的间隔时间
	/// </summary>
	public float cullInterval = 1f;
	/// <summary>
	/// 对象池的名字
	/// </summary>
	public string name
	{
		get;
		private set;
	}
	private bool m_autoRelease = false;
	private object m_lock_autoRelease = new object();
	#region 接口函数
	public bool autoRelease
	{
		get
		{
			return m_autoRelease;
		}
		set
		{
			if(m_autoRelease != value)
			{
				if(value)
				{
					PAPoolManager.instance.AddAutoPoolCall(autocall);
				}
				else
				{
					PAPoolManager.instance.RemoveAutoPoolCall(autocall);
				}
				m_autoRelease = value;
			}
		}
	}
	public CobjectPool()
	{
		Init();
	}
	public CobjectPool(int preloadAmount,int cullAbove)
	{
		Init();
		this.preLoadAmount = preloadAmount;
		this.cullAbove = cullAbove;
	}
	/// <summary>
	/// 从对象池中生成一个对象
	/// </summary>
	/// <returns>从对象池生成的对象 失败则返回null</returns>
	public T Spawn()
	{
		T t = default(T);
		if(_dispawn.Count > 0)
		{
			t = SpawnInstance();
		}
		else
		{
			t = SpawnNewInstance();
		}
		//
		if(t == null)
		{
			Log("Spawn failed return null");
			return t;
		}
		lock(_spawned)
		{
			_spawned.Add(t);
		}
		return t;
	}
	/// <summary>
	/// 释放对象,使其回到对象池
	/// </summary>
	/// <param name="t">需要释放的对象</param>
	/// <returns></returns>
	public bool Despawn(T t)
	{
		if(t == null)
		{
			Log("can not Despawn null object");
			return false;
		}
		bool b = false;

		lock(_spawned)
		{
			b = _spawned.Remove(t);
		}
		if(!b)
		{
			Log("object can exit in spawned");
			return false;
		}
		lock(_dispawn)
		{
			_dispawn.Enqueue(t);
		}
		return true;
	}
	/// <summary>
	/// 释放多余的未使用对象
	/// </summary>
	public void ReleaseAxcess()
	{
		lock(_dispawn)
		{
			int total = Length();
			while(true)
			{
				if(total <= cullAbove || _dispawn.Count == 0)
				{
					break;
				}
				_dispawn.Dequeue();
			}
		}
	}
	public int Length()
	{
		return _spawned.Count + _dispawn.Count;
	}
	#endregion
	#region 私有函数
	private void Init()
	{
		Type t = typeof(T);
		name = t.FullName;
		logPrefix = string.Format("named [{0}] CojbectPool :",name);
		autocall = new Callback(this.ReleaseAxcessDelay);
	}
	/// <summary>
	/// 以string.format()的格式打印日志并添加特殊前缀
	/// </summary>
	/// <param name="format"></param>
	/// <param name="args"></param>
	private void Log(string format,params object[] args)
	{
		string s = string.Format(format,args);
		Debug.LogWarning(logPrefix + s);
	}
	/// <summary>
	/// 从对象池中拿出一个已经使用过的对象
	/// </summary>
	/// <returns>对象实例</returns>
	private T SpawnInstance()
	{
		T t = _dispawn.Dequeue();
		return t;
	}
	/// <summary>
	/// 生成一个新对象
	/// </summary>
	/// <returns>新对象的实例,创建不成功return null</returns>
	private T SpawnNewInstance()
	{
		T t = new T();
		if(t == null)
		{
			return t;
		}
		return t;
	}
	private void ReleaseAxcessDelay()
	{
		elapse += Time.deltaTime;
		if(elapse >= cullInterval)
		{
			elapse -= cullInterval;
			ReleaseAxcess();
		}
	}
	#endregion
}
