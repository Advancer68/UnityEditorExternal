﻿using UnityEngine;
using System.Collections;
using System;
using CDebug = System.Diagnostics.Debug;

public class PAPoolManager:MonoSingletonC<PAPoolManager>
{
	// Use this for initialization
	private Callback autoReleaseCall = null;
	#region 接口函数
	public void AddAutoPoolCall(Callback call)
	{
		CDebug.Assert(call == null,"call can not null");
		Delegate[] dlist = autoReleaseCall.GetInvocationList();
		if(PAPoolTool.Contain<Delegate>(dlist,call))
		{
			Debug.LogWarning("call contain the PAPoolManager");
			return;
		}
		lock(autoReleaseCall)
		{
			autoReleaseCall += call;
		}
		this.enabled = true;
	}
	public void RemoveAutoPoolCall(Callback call)
	{
		CDebug.Assert(call == null,"call can not null");
		Delegate[] dlist = autoReleaseCall.GetInvocationList();
		if(!PAPoolTool.Contain<Delegate>(dlist,call))
		{
			Debug.LogWarning("call contain the PAPoolManager");
			return;
		}
		lock(autoReleaseCall)
		{
			autoReleaseCall -= call;
		}
		if(dlist.Length <= 1)
		{
			this.enabled = false;
		}
	}
	#endregion
	#region 私有函数
	public override void Init()
	{
		base.Init();
		this.enabled = false;
		if(autoReleaseCall == null)
		{
			autoReleaseCall = new Callback(this.UpdatePre);
		}
	}
	void UpdatePre()
	{
	}
	// Update is called once per frame
	void Update()
	{
		autoReleaseCall();
	}
	#endregion
}
public class PAPoolTool
{
	public static bool Contain<T>(T[] list,T t)
	{
		if(t == null || list == null)
		{
			return false;
		}
		foreach(T item in list)
		{
			if(item.Equals(t))
			{
				return true;
			}
		}
		return false;
	}
}