﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class PAToolsEditor{
	private static System.Object obj;
	private static Hashtable t = new Hashtable();
	private static int index = 0;
	public class PAParam
	{
		public object value;
		public PAParam(object v)
		{
			SetValue(v);
		}
		public void SetValue(object v)
		{
			value = v;
		}
		public static implicit operator int(PAParam p)
		{
			return (int)p.value;
		}
		public static implicit operator string(PAParam p)
		{
			return (string)p.value;
		}
		public override string ToString()
		{
			return value.ToString();
		}
	}
	[MenuItem("PATOOL/Link Material")]
	static void LinkMater()
	{
		Object[] materials = Selection.GetFiltered(typeof(Material),SelectionMode.Unfiltered);
		Object[] textures = Selection.GetFiltered(typeof(AudioClip),SelectionMode.DeepAssets);
		if(materials.Length == 0 || textures.Length == 0)
		{
			Debug.Log("not select material");
		}
		Dictionary<string,Object> textureMap = new Dictionary<string,Object>(textures.Length);
		foreach(Object item in textures)
		{
			textureMap[item.name] = item;
		}
		materials = Selection.objects;
		//string dictor = AssetDatabase.GetAssetPath(materials[0]);
	}
	[MenuItem("PATOOL/TestDebug")]
	static void TestDebug()
	{
		Debug.Log(obj);
		object o = t[0];
		Debug.Log(o);
	}
	[MenuItem("PATOOL/TestSetInt")]
	static void TestsetInt()
	{
		PAParam p = obj as PAParam;
		p.SetValue(index + 0.2f);
	}
	[MenuItem("PATOOL/TestSetBool")]
	static void TestsetBool()
	{
		PAParam p = obj as PAParam;
		p.SetValue(false);
	}
	[MenuItem("PATOOL/TestSetfloat")]
	static void Testsetfloat()
	{
		PAParam p = obj as PAParam;
		p.SetValue(index + 0.2f);
	}
	[MenuItem("PATOOL/TestAdd")]
	static void TestAdd()
	{
		t.Add(index,new PAParam(index));
		obj = t[0];
		index++;
	}
	[MenuItem("PATOOL/Testfuzhi")]
	static void TestAddfuzhi()
	{
		obj = t[0];
	}
}
