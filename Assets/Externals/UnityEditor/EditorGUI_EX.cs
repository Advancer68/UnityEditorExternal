﻿namespace UnityEditor
{
	using System;
	using System.Collections.Generic;
	using System.Globalization;
	using System.Linq;
	using System.Runtime.CompilerServices;
	using System.Runtime.InteropServices;
	using UnityEditorInternal;
	using UnityEngine;
	using UnityEngine.Internal;
	public sealed class EditorGUI_EX
	{
		private static int s_ArraySizeFieldHash = "ArraySizeField".GetHashCode();
		private static int s_ButtonMouseDownHash = "ButtonMouseDown".GetHashCode();
		private static GUIContent s_MixedValueContent = new GUIContent("—","Mixed Values");
		private static Color s_MixedValueContentColor = new Color(1f,1f,1f,0.5f);
		private static Color s_MixedValueContentColorTemp = Color.white;
		static void BeginHandleMixedValueContentColor()
		{
			s_MixedValueContentColorTemp = GUI.contentColor;
			GUI.contentColor = !EditorGUI.showMixedValue ? GUI.contentColor : (GUI.contentColor * s_MixedValueContentColor);
		}
		static void EndHandleMixedValueContentColor()
		{
			GUI.contentColor = s_MixedValueContentColorTemp;
		}
		public static bool ButtonMouseDown(Rect position,GUIContent content,FocusType fType,GUIStyle style)
		{
			int controlID = GUIUtility.GetControlID(s_ButtonMouseDownHash,fType,position);
			Event current = Event.current;
			EventType type = current.type;
			switch(type)
			{
				case EventType.KeyDown:
					if((GUIUtility.keyboardControl != controlID) || (current.character != ' '))
					{
						break;
					}
					Event.current.Use();
					return true;

				case EventType.Repaint:
					if(!EditorGUI.showMixedValue)
					{
						style.Draw(position,content,controlID,false);
					}
					else
					{
						BeginHandleMixedValueContentColor();
						style.Draw(position,s_MixedValueContent,controlID,false);
						EndHandleMixedValueContentColor();
					}
					break;

				default:
					if((type == EventType.MouseDown) && (position.Contains(current.mousePosition) && (current.button == 0)))
					{
						Event.current.Use();
						return true;
					}
					break;
			}
			return false;
		}
	}
}