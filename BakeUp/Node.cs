﻿namespace UnityEditor.PAGraphs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using UnityEditor;
    using UnityEngine;
	//using UnityEditor.Graphs;

    public class Node : ScriptableObject
    {
        [CompilerGenerated]
        private static Func<Slot, bool> cache10;
        [CompilerGenerated]
        private static Func<Slot, bool> cache11;
        [CompilerGenerated]
        private static Func<Slot, bool> cache12;
        [CompilerGenerated]
        private static Func<Slot, bool> cache13;
        [CompilerGenerated]
        private static Func<Slot, bool> cache14;
        [CompilerGenerated]
        private static Func<Slot, IEnumerable<Edge>> cache15;
        [CompilerGenerated]
        private static Func<Slot, IEnumerable<Edge>> cache16;
        [CompilerGenerated]
        private static Func<Slot, IEnumerable<Edge>> cache17;
        [CompilerGenerated]
        private static Func<Slot, IEnumerable<Edge>> cache18;
        [CompilerGenerated]
        private static Func<Slot, IEnumerable<Edge>> cache19;
        [CompilerGenerated]
        private static Func<Slot, IEnumerable<Edge>> cache1A;
        [CompilerGenerated]
        private static Func<Slot, bool> cache1B;
        [CompilerGenerated]
        private static Func<Slot, bool> cache1C;
        [CompilerGenerated]
        private static Func<Slot, bool> cache1D;
        [CompilerGenerated]
        private static Func<string, Slot, string> cache1E;
        [CompilerGenerated]
        private static Func<string, Slot, string> cache1F;
        [CompilerGenerated]
        private static Func<Slot, bool> cacheE;
        [CompilerGenerated]
        private static Func<Slot, bool> cacheF;
        [SerializeField]
        public UnityEditor.Graphs.Styles.Color color;
        [SerializeField]
        protected string m_GenericTypeString = string.Empty;
        [NonSerialized]
        internal Graph m_Graph;
        [NonSerialized]
        internal List<int> m_HiddenProperties = new List<int>();
        private bool m_IsDragging;
        private string m_NodeInvalidError = string.Empty;
        [SerializeField]
        internal List<Property> m_Properties = new List<Property>();
        [NonSerialized]
        internal List<int> m_SettingProperties = new List<int>();
        [NonSerialized]
        internal List<string> m_SettingPropertyTitles = new List<string>();
        [SerializeField]
        protected List<Slot> m_Slots = new List<Slot>();
        [NonSerialized]
        private string m_Title = string.Empty;
        [SerializeField]
        public Rect position;
        [SerializeField]
        internal bool showEmptySlots = true;
        [SerializeField]
        public string style = "node";

        public virtual void AddedToGraph()
        {
        }

        public Slot AddInputSlot(string name)
        {
            return this.AddInputSlot(name, null);
        }

        public Slot AddInputSlot(string name, System.Type type)
        {
            Slot s = new Slot(SlotType.InputSlot, name, type);
            this.AddSlot(s);
            return s;
        }

        public Property AddOrModifyProperty(System.Type dataType, string name)
        {
            Property property = this.TryGetProperty(name);
            if (property != null)
            {
                if (!property.isGeneric && (property.type != dataType))
                {
                    property.ChangeDataType(dataType);
                }
                return property;
            }
            return this.ConstructAndAddProperty(dataType, name);
        }

        public Property AddOrModifyPropertyForSlot(Slot s)
        {
            Property property = this.TryGetProperty(s.name);
            if (property != null)
            {
                if (!s.isGeneric && (property.type != s.dataType))
                {
                    property.ChangeDataType(s.dataType);
                }
                return property;
            }
            return this.ConstructAndAddProperty(s.dataTypeString, s.name);
        }

        public Slot AddOutputSlot(string name)
        {
            return this.AddOutputSlot(name, null);
        }

        public Slot AddOutputSlot(string name, System.Type type)
        {
            Slot s = new Slot(SlotType.OutputSlot, name, type);
            this.AddSlot(s);
            return s;
        }

        public void AddProperty(Property p)
        {
            this.AssertNotDuplicateName(p.name);
            this.m_Properties.Add(p);
            this.Dirty();
        }

        public void AddSlot(Slot s)
        {
            this.AddSlot(s, -1);
        }

        public virtual void AddSlot(Slot s, int index)
        {
            if (index != -1)
            {
                this.m_Slots.Insert(index, s);
            }
            else
            {
                this.m_Slots.Add(s);
            }
            if ((s.type == SlotType.InputSlot) && !s.isFlowSlot)
            {
                this.AddOrModifyPropertyForSlot(s);
            }
            if (s.node != null)
            {
                throw new ArgumentException("Slot already attached to another node");
            }
            s.node = this;
            this.Dirty();
        }

        private void AssertNotDuplicateName(string name)
        {
            if (this.TryGetProperty(name) != null)
            {
                throw new ArgumentException("Property '" + name + "' already exists.");
            }
        }

        internal virtual void Awake()
        {
        }

        public virtual void BeginDrag()
        {
        }

        public virtual void ChangeSlotType(Slot s, System.Type toType)
        {
            if (s.dataType != toType)
            {
                s.dataType = toType;
                if (s.isInputDataSlot)
                {
                    this.GetProperty(s.name).ChangeDataType(toType);
                }
                if ((this.graph != null) && s.isDataSlot)
                {
                    if (s.type == SlotType.InputSlot)
                    {
                        this.graph.RevalidateInputDataEdges(s);
                    }
                    else
                    {
                        this.graph.RevalidateOutputDataEdges(s);
                    }
                }
                this.Dirty();
            }
        }

        public Property ConstructAndAddProperty(string serializedTypeString, string name)
        {
            Property p = new Property(serializedTypeString, name);
            this.AddProperty(p);
            return p;
        }

        public Property ConstructAndAddProperty(System.Type type, string name)
        {
            Property p = new Property(type, name);
            this.AddProperty(p);
            return p;
        }

        public virtual void Dirty()
        {
            EditorUtility.SetDirty(this);
        }

        public virtual void EndDrag()
        {
            this.m_IsDragging = false;
        }

        public Property GetOrCreateAndAddProperty(System.Type type, string name)
        {
            Property property = this.TryGetProperty(name);
            if (property == null)
            {
                return this.ConstructAndAddProperty(type, name);
            }
            property.ChangeDataType(type);
            return property;
        }

        public Property GetProperty(string name)
        {
            Property property = this.TryGetProperty(name);
            if (property == null)
            {
                throw new ArgumentException("Property '" + name + "' not found.");
            }
            return property;
        }

        public object GetPropertyValue(string name)
        {
            return this.GetProperty(name).value;
        }

        public string GetSettingPropertyTitle(Property property)
        {
            int index = this.m_Properties.IndexOf(property);
            if (index == -1)
            {
                return string.Empty;
            }
            index = this.m_SettingProperties.IndexOf(index);
            if (index == -1)
            {
                return string.Empty;
            }
            return this.m_SettingPropertyTitles[index];
        }

        public object GetSlotValue(string slotName)
        {
            return this.GetPropertyValue(slotName);
        }

        internal void HideProperty(Property p)
        {
            int index = this.properties.IndexOf(p);
            if (index == -1)
            {
                throw new ArgumentException("Could not find property to hide.");
            }
            this.m_HiddenProperties.Add(index);
            this.Dirty();
        }

        public virtual void InputEdgeChanged(Edge e)
        {
        }

        public static Node Instance()
        {
            return Instance<Node>();
        }

        public static T Instance<T>() where T: Node, new()
        {
            return ScriptableObject.CreateInstance<T>();
        }

        internal bool IsPropertyHidden(Property p)
        {
            int index = this.properties.IndexOf(p);
            if (index == -1)
            {
                return false;
            }
            return this.m_HiddenProperties.Contains(index);
        }

        internal void MakeSettingProperty(Property p, string title)
        {
            int index = this.properties.IndexOf(p);
            if (index == -1)
            {
                throw new ArgumentException("Failed to find property to turn into a setting property.");
            }
            this.m_SettingProperties.Add(index);
            this.m_SettingPropertyTitles.Add(title);
            this.Dirty();
        }

        public virtual void NodeUI(GraphGUI host)
        {
        }

        public virtual void OnDrag()
        {
            this.m_IsDragging = true;
        }

        public void RemoveProperty(string name)
        {
            Property p = this.TryGetProperty(name);
            if (p == null)
            {
                Debug.LogError("Trying to remove non-existant property " + name);
            }
            else
            {
                this.RemoveProperty(p);
            }
        }

        public void RemoveProperty(Property p)
        {
            if (!this.m_Properties.Contains(p))
            {
                Debug.LogError("Trying to remove non-existant property " + base.name);
            }
            else
            {
                int index = this.m_Properties.IndexOf(p);
                this.m_Properties.RemoveAt(index);
                int num2 = this.m_SettingProperties.IndexOf(index);
                if (num2 != -1)
                {
                    this.m_SettingProperties.RemoveAt(num2);
                    this.m_SettingPropertyTitles.RemoveAt(num2);
                }
                int num3 = this.m_HiddenProperties.IndexOf(index);
                if (num3 != -1)
                {
                    this.m_HiddenProperties.RemoveAt(num3);
                }
                this.Dirty();
            }
        }

        public virtual void RemoveSlot(Slot s)
        {
            this.m_Slots.Remove(s);
            foreach (Edge edge in s.edges)
            {
                if (edge.fromSlot != s)
                {
                    edge.fromSlot.RemoveEdge(edge);
                }
                if (edge.toSlot != s)
                {
                    edge.toSlot.RemoveEdge(edge);
                }
                this.graph.edges.Remove(edge);
                this.graph.Dirty();
            }
            s.edges.Clear();
            if (this.graph != null)
            {
                this.graph.RemoveInvalidEdgesForSlot(s);
            }
            this.Dirty();
        }

        public virtual void RemovingFromGraph()
        {
        }

        public virtual void RenameProperty(string oldName, string newName, System.Type newType)
        {
            Property property = this.GetProperty(oldName);
            property.name = newName;
            property.ChangeDataType(newType);
            this.Dirty();
        }

        public virtual void ResetGenericPropertyArgumentType()
        {
            this.m_GenericTypeString = string.Empty;
            foreach (Slot slot in this.slots)
            {
                if (slot.isGeneric && slot.isInputDataSlot)
                {
                    this.GetProperty(slot.name).ResetGenericArgumentType();
                    slot.ResetGenericArgumentType();
                    while (slot.edges.Any<Edge>())
                    {
                        this.graph.RemoveEdge(slot.edges.First<Edge>());
                    }
                }
            }
            if (cache1D == null)
            {
                cache1D = s => s.isGeneric && s.isOutputDataSlot;
            }
            IEnumerator<Slot> enumerator = this.slots.Where<Slot>(cache1D).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    Slot current = enumerator.Current;
                    current.ResetGenericArgumentType();
                    while (current.edges.Any<Edge>())
                    {
                        this.graph.RemoveEdge(current.edges.First<Edge>());
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            this.Dirty();
        }

        public virtual void SetGenericPropertyArgumentType(System.Type type)
        {
            if (cache1B == null)
            {
                cache1B = s => s.isGeneric;
            }
            IEnumerator<Slot> enumerator = this.inputDataSlots.Where<Slot>(cache1B).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    Slot current = enumerator.Current;
                    Property property = this.GetProperty(current.name);
                    current.SetGenericArgumentType(type);
                    property.SetGenericArgumentType(type);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            if (cache1C == null)
            {
                cache1C = s => s.isGeneric;
            }
            IEnumerator<Slot> enumerator2 = this.outputDataSlots.Where<Slot>(cache1C).GetEnumerator();
            try
            {
                while (enumerator2.MoveNext())
                {
                    enumerator2.Current.SetGenericArgumentType(type);
                }
            }
            finally
            {
                if (enumerator2 == null)
                {
                }
                enumerator2.Dispose();
            }
            this.genericType = type;
            this.Dirty();
        }

        public void SetPropertyValue(string name, object value)
        {
            this.GetProperty(name).value = value;
            this.Dirty();
        }

        public void SetPropertyValueOrCreateAndAddProperty(string name, System.Type type, object value)
        {
            Property p = this.TryGetProperty(name);
            if (p == null)
            {
                p = new Property(type, name);
                this.AddProperty(p);
            }
            else
            {
                p.ChangeDataType(type);
            }
            p.value = value;
            this.Dirty();
        }

        public override string ToString()
        {
            string seed = ("[" + base.GetType().Name + "   " + this.title) + "| ";
            if (cache1E == null)
            {
                cache1E = (current, slot) => current + "o[" + slot.name + "]";
            }
            seed = this.outputSlots.Aggregate<Slot, string>(seed, cache1E);
            if (cache1F == null)
            {
                cache1F = (current, slot) => current + "i[" + slot.name + "]";
            }
            return (this.outputSlots.Aggregate<Slot, string>(seed, cache1F) + "]");
        }

        public Property TryGetProperty(string name)
        {
            c__AnonStoreyB yb = new c__AnonStoreyB {
                name = name
            };
            return this.m_Properties.FirstOrDefault<Property>(new Func<Property, bool>(yb.m__21));
        }

        public object TryGetSlotPropertyValue(Slot slot)
        {
            Property property = this.TryGetProperty(slot.name);
            return ((property != null) ? property.value : null);
        }

        internal virtual void WakeUp(Graph owner)
        {
            this.m_Graph = owner;
            if (this.m_Slots == null)
            {
                Debug.LogError("Slots are null - should not happen");
                this.m_Slots = new List<Slot>();
            }
            foreach (Slot slot in this.slots)
            {
                if (slot != null)
                {
                    slot.WakeUp(this);
                }
                else
                {
                    Debug.LogError("NULL SLOT");
                }
            }
        }

        public System.Type genericType
        {
            get
            {
                return SerializedType.FromString(this.m_GenericTypeString);
            }
            set
            {
                this.m_GenericTypeString = SerializedType.ToString(value);
            }
        }

        public Graph graph
        {
            get
            {
                return this.m_Graph;
            }
            set
            {
                this.m_Graph = value;
            }
        }

        public bool hasTitle
        {
            get
            {
                return !string.IsNullOrEmpty(this.m_Title);
            }
        }

        public IEnumerable<Edge> inputDataEdges
        {
            get
            {
                if (cache1A == null)
                {
                    cache1A = s => s.edges;
                }
                return this.inputDataSlots.SelectMany<Slot, Edge>(cache1A);
            }
        }

        public IEnumerable<Slot> inputDataSlots
        {
            get
            {
                if (cache11 == null)
                {
                    cache11 = s => ((s.type == SlotType.InputSlot) && s.isDataSlot) && (s.name != "Target");
                }
                return this.m_Slots.Where<Slot>(cache11);
            }
        }

        public IEnumerable<Edge> inputEdges
        {
            get
            {
                if (cache16 == null)
                {
                    cache16 = s => s.edges;
                }
                return this.inputSlots.SelectMany<Slot, Edge>(cache16);
            }
        }

        public IEnumerable<Edge> inputFlowEdges
        {
            get
            {
                if (cache18 == null)
                {
                    cache18 = s => s.edges;
                }
                return this.inputFlowSlots.SelectMany<Slot, Edge>(cache18);
            }
        }

        public IEnumerable<Slot> inputFlowSlots
        {
            get
            {
                if (cache13 == null)
                {
                    cache13 = s => (s.type == SlotType.InputSlot) && s.isFlowSlot;
                }
                return this.m_Slots.Where<Slot>(cache13);
            }
        }

        public IEnumerable<Slot> inputSlots
        {
            get
            {
                if (cacheF == null)
                {
                    cacheF = s => s.type == SlotType.InputSlot;
                }
                return this.m_Slots.Where<Slot>(cacheF);
            }
        }

        public bool isDragging
        {
            get
            {
                return this.m_IsDragging;
            }
        }

        public bool isGeneric
        {
            get
            {
                if (cacheE == null)
                {
                    cacheE = s => s.isGeneric;
                }
                return this.slots.Any<Slot>(cacheE);
            }
        }

        public Slot this[string name]
        {
            get
            {
                c__AnonStoreyA ya = new c__AnonStoreyA {
                    name = name
                };
                return this.m_Slots.FirstOrDefault<Slot>(new Func<Slot, bool>(ya.m__1B));
            }
        }

        public Slot this[int index]
        {
            get
            {
                return this.m_Slots[index];
            }
        }

        public string nodeInvalidError
        {
            get
            {
                return this.m_NodeInvalidError;
            }
            set
            {
                this.m_NodeInvalidError = value;
            }
        }

        public bool nodeIsInvalid
        {
            get
            {
                return (this.m_NodeInvalidError != string.Empty);
            }
        }

        public IEnumerable<Edge> outputDataEdges
        {
            get
            {
                if (cache19 == null)
                {
                    cache19 = s => s.edges;
                }
                return this.outputDataSlots.SelectMany<Slot, Edge>(cache19);
            }
        }

        public IEnumerable<Slot> outputDataSlots
        {
            get
            {
                if (cache12 == null)
                {
                    cache12 = s => (s.type == SlotType.OutputSlot) && s.isDataSlot;
                }
                return this.m_Slots.Where<Slot>(cache12);
            }
        }

        public IEnumerable<Edge> outputEdges
        {
            get
            {
                if (cache15 == null)
                {
                    cache15 = s => s.edges;
                }
                return this.outputSlots.SelectMany<Slot, Edge>(cache15);
            }
        }

        public IEnumerable<Edge> outputFlowEdges
        {
            get
            {
                if (cache17 == null)
                {
                    cache17 = s => s.edges;
                }
                return this.outputFlowSlots.SelectMany<Slot, Edge>(cache17);
            }
        }

        public IEnumerable<Slot> outputFlowSlots
        {
            get
            {
                if (cache14 == null)
                {
                    cache14 = s => (s.type == SlotType.OutputSlot) && s.isFlowSlot;
                }
                return this.m_Slots.Where<Slot>(cache14);
            }
        }

        public IEnumerable<Slot> outputSlots
        {
            get
            {
                if (cache10 == null)
                {
                    cache10 = s => s.type == SlotType.OutputSlot;
                }
                return this.m_Slots.Where<Slot>(cache10);
            }
        }

        public List<Property> properties
        {
            get
            {
                return this.m_Properties;
            }
        }

        public IEnumerable<Property> settingProperties
        {
            get
            {
                return (from i in this.m_SettingProperties select this.m_Properties[i]);
            }
        }

        public List<Slot> slots
        {
            get
            {
                return this.m_Slots;
            }
        }

        public virtual string title
        {
            get
            {
                return (!(this.m_Title == string.Empty) ? this.m_Title : base.name);
            }
            set
            {
                this.m_Title = value;
                this.Dirty();
            }
        }

        public virtual string windowTitle
        {
            get
            {
                return base.GetType().Name;
            }
        }

        [CompilerGenerated]
        private sealed class c__AnonStoreyA
        {
            internal string name;

            internal bool m__1B(Slot s)
            {
                return (s.name == this.name);
            }
        }

        [CompilerGenerated]
        private sealed class c__AnonStoreyB
        {
            internal string name;

            internal bool m__21(Property p)
            {
                return (p.name == this.name);
            }
        }
    }
}

