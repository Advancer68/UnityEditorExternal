﻿namespace UnityEditor.PAGraphs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEditor;
    using UnityEngine;
	using UnityEditor.Graphs;

    [Serializable]
    public class Slot
    {
        [CompilerGenerated]
        private static Dictionary<string, int> map2;
        [NonSerialized]
        public List<Edge> edges;
        [SerializeField]
        private string m_DataTypeString;
        [SerializeField]
        private string m_Name;
        [NonSerialized]
        private Node m_Node;
        [NonSerialized]
        internal Rect m_Position;
        [SerializeField]
        private string m_Title;
        public SlotType type;

        public Slot()
        {
            this.m_Title = string.Empty;
            this.m_Name = string.Empty;
            this.m_DataTypeString = string.Empty;
            this.Init();
        }

        public Slot(SlotType type)
        {
            this.m_Title = string.Empty;
            this.m_Name = string.Empty;
            this.m_DataTypeString = string.Empty;
            this.Init();
            this.type = type;
        }

        public Slot(SlotType type, string name)
        {
            this.m_Title = string.Empty;
            this.m_Name = string.Empty;
            this.m_DataTypeString = string.Empty;
            this.Init();
            this.name = name;
            this.type = type;
        }

        public Slot(SlotType type, string name, string title)
        {
            this.m_Title = string.Empty;
            this.m_Name = string.Empty;
            this.m_DataTypeString = string.Empty;
            this.Init();
            this.name = name;
            this.type = type;
            this.title = title;
        }

        public Slot(SlotType type, string name, System.Type dataType)
        {
            this.m_Title = string.Empty;
            this.m_Name = string.Empty;
            this.m_DataTypeString = string.Empty;
            this.Init();
            this.name = name;
            this.type = type;
            this.dataType = dataType;
        }

        public Slot(SlotType type, string name, string title, System.Type dataType)
        {
            this.m_Title = string.Empty;
            this.m_Name = string.Empty;
            this.m_DataTypeString = string.Empty;
            this.Init();
            this.name = name;
            this.type = type;
            this.title = title;
            this.dataType = dataType;
        }

        public void AddEdge(Edge e)
        {
            if (this.edges == null)
            {
                throw new NullReferenceException("WTF - Edges are null?");
            }
            this.edges.Add(e);
        }

        private static string GetNiceTitle(string name)
        {
            string key = name;
            if (key != null)
            {
                int num;
                if (map2 == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(5);
                    dictionary.Add("Target", 0);
                    dictionary.Add("FnIn", 1);
                    dictionary.Add("FnOut", 2);
                    dictionary.Add("VarIn", 3);
                    dictionary.Add("VarOut", 4);
                    map2 = dictionary;
                }
                if (map2.TryGetValue(key, out num))
                {
                    switch (num)
                    {
                        case 0:
                            return string.Empty;

                        case 1:
                            return "In";

                        case 2:
                            return "Out";

                        case 3:
                            return "Param";

                        case 4:
                            return "Value";
                    }
                }
            }
            return ObjectNames.NicifyVariableName(name);
        }

        public Property GetProperty()
        {
            return this.node.GetProperty(this.name);
        }

        internal bool HasConnectionTo(Slot toSlot)
        {
            c__AnonStoreyC yc = new c__AnonStoreyC {
                toSlot = toSlot
            };
            return this.edges.Any<Edge>(new Func<Edge, bool>(yc.m__27));
        }

        private void Init()
        {
            this.edges = new List<Edge>();
        }

        public void RemoveEdge(Edge e)
        {
            this.edges.Remove(e);
        }

        public void ResetGenericArgumentType()
        {
            this.m_DataTypeString = SerializedType.ResetGenericArgumentType(this.m_DataTypeString);
        }

        public void SetGenericArgumentType(System.Type type)
        {
            this.m_DataTypeString = SerializedType.SetGenericArgumentType(this.m_DataTypeString, type);
        }

        public override string ToString()
        {
            return this.name;
        }

        internal void WakeUp(Node owner)
        {
            if (this.edges == null)
            {
                this.edges = new List<Edge>();
            }
            this.m_Node = owner;
        }

        public System.Type dataType
        {
            get
            {
                return SerializedType.FromString(this.m_DataTypeString);
            }
            set
            {
                this.m_DataTypeString = SerializedType.ToString(value);
            }
        }

        public string dataTypeString
        {
            get
            {
                return this.m_DataTypeString;
            }
        }

        public bool isDataSlot
        {
            get
            {
                return !string.IsNullOrEmpty(this.m_DataTypeString);
            }
        }

        public bool isFlowSlot
        {
            get
            {
                return (!this.isDataSlot && !this.isTarget);
            }
        }

        public bool isGeneric
        {
            get
            {
                return SerializedType.IsBaseTypeGeneric(this.m_DataTypeString);
            }
        }

        public bool isInputDataSlot
        {
            get
            {
                return (this.isDataSlot && (this.type == SlotType.InputSlot));
            }
        }

        public bool isInputSlot
        {
            get
            {
                return (this.type == SlotType.InputSlot);
            }
        }

        public bool isOutputDataSlot
        {
            get
            {
                return (this.isDataSlot && (this.type == SlotType.OutputSlot));
            }
        }

        public bool isOutputSlot
        {
            get
            {
                return (this.type == SlotType.OutputSlot);
            }
        }

        public bool isTarget
        {
            get
            {
                return (this.name == "Target");
            }
        }

        public string name
        {
            get
            {
                return this.m_Name;
            }
            set
            {
                this.m_Name = value;
                this.m_Title = GetNiceTitle(this.m_Name);
            }
        }

        public Node node
        {
            get
            {
                return this.m_Node;
            }
            set
            {
                this.m_Node = value;
            }
        }

        public string title
        {
            get
            {
                return this.m_Title;
            }
            set
            {
                this.m_Title = value;
            }
        }

        [CompilerGenerated]
        private sealed class c__AnonStoreyC
        {
            internal Slot toSlot;

            internal bool m__27(Edge e)
            {
                return (e.toSlot == this.toSlot);
            }
        }
    }
}

