﻿namespace UnityEngineEX
{
    using System;

    public sealed class GUILayoutOption
    {
		public Type type;
        public object value;

        internal GUILayoutOption(Type type, object value)
        {
            this.type = type;
            this.value = value;
        }

        public enum Type
        {
            fixedWidth,
            fixedHeight,
            minWidth,
            maxWidth,
            minHeight,
            maxHeight,
            stretchWidth,
            stretchHeight,
            alignStart,
            alignMiddle,
            alignEnd,
            alignJustify,
            equalSize,
            spacing
        }
    }
}

