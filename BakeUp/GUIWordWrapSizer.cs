﻿namespace UnityEngineEX
{
    using System;
	using UnityEngine;

    internal sealed class GUIWordWrapSizer : GUILayoutEntry
    {
        private GUIContent content;
        private float forcedMaxHeight;
        private float forcedMinHeight;

        public GUIWordWrapSizer(GUIStyle _style, GUIContent _content, GUILayoutOption[] options) : base(0f, 0f, 0f, 0f, _style)
        {
            this.content = new GUIContent(_content);
            base.ApplyOptions(options);
            this.forcedMinHeight = base.minHeight;
            this.forcedMaxHeight = base.maxHeight;
        }

        public override void CalcHeight()
        {
            if ((this.forcedMinHeight == 0f) || (this.forcedMaxHeight == 0f))
            {
                float num = base.style.CalcHeight(this.content, this.rect.width);
                if (this.forcedMinHeight == 0f)
                {
                    base.minHeight = num;
                }
                else
                {
                    base.minHeight = this.forcedMinHeight;
                }
                if (this.forcedMaxHeight == 0f)
                {
                    base.maxHeight = num;
                }
                else
                {
                    base.maxHeight = this.forcedMaxHeight;
                }
            }
        }

        public override void CalcWidth()
        {
            if ((base.minWidth == 0f) || (base.maxWidth == 0f))
            {
                float num;
                float num2;
                base.style.CalcMinMaxWidth(this.content, out num, out num2);
                if (base.minWidth == 0f)
                {
                    base.minWidth = num;
                }
                if (base.maxWidth == 0f)
                {
                    base.maxWidth = num2;
                }
            }
        }
    }
}

