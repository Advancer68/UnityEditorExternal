﻿namespace UnityEngineEX
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Security;
    using UnityEngineInternal;
	using UnityEngine;

    public class GUILayoutUtility
    {
        internal static LayoutCache current = new LayoutCache();
        private static Rect kDummyRect = new Rect(0f, 0f, 1f, 1f);
        private static GUIStyle s_SpaceStyle;
        private static Dictionary<int, LayoutCache> storedLayouts = new Dictionary<int, LayoutCache>();
        private static Dictionary<int, LayoutCache> storedWindows = new Dictionary<int, LayoutCache>();

        internal static void Begin(int instanceID)
        {
            LayoutCache cache = SelectIDList(instanceID, false);
            if (Event.current.type == EventType.Layout)
            {
                current.topLevel = cache.topLevel = new GUILayoutGroup();
                current.layoutGroups.Clear();
                current.layoutGroups.Push(current.topLevel);
                current.windows = cache.windows = new GUILayoutGroup();
            }
            else
            {
                current.topLevel = cache.topLevel;
                current.layoutGroups = cache.layoutGroups;
                current.windows = cache.windows;
            }
        }

        public static void BeginGroup(string GroupName)
        {
        }

        internal static GUILayoutGroup BeginLayoutArea(GUIStyle style, Type LayoutType)
        {
            GUILayoutGroup next;
            switch (Event.current.type)
            {
                case EventType.Layout:
                case EventType.Used:
                    next = CreateGUILayoutGroupInstanceOfType(LayoutType);
                    next.style = style;
                    current.windows.Add(next);
                    break;

                default:
                    next = current.windows.GetNext() as GUILayoutGroup;
                    if (next == null)
                    {
                        throw new ArgumentException("GUILayout: Mismatched LayoutGroup." + Event.current.type);
                    }
                    next.ResetCursor();
                    break;
            }
            current.layoutGroups.Push(next);
            current.topLevel = next;
            return next;
        }

        internal static GUILayoutGroup BeginLayoutGroup(GUIStyle style, GUILayoutOption[] options, Type LayoutType)
        {
            GUILayoutGroup next;
            switch (Event.current.type)
            {
                case EventType.Layout:
                case EventType.Used:
                    next = CreateGUILayoutGroupInstanceOfType(LayoutType);
                    next.style = style;
                    if (options != null)
                    {
                        next.ApplyOptions(options);
                    }
                    current.topLevel.Add(next);
                    break;

                default:
                    next = current.topLevel.GetNext() as GUILayoutGroup;
                    if (next == null)
                    {
                        throw new ArgumentException("GUILayout: Mismatched LayoutGroup." + Event.current.type);
                    }
                    next.ResetCursor();
                    break;
            }
            current.layoutGroups.Push(next);
            current.topLevel = next;
            return next;
        }

        internal static void BeginWindow(int windowID, GUIStyle style, GUILayoutOption[] options)
        {
            LayoutCache cache = SelectIDList(windowID, true);
            if (Event.current.type == EventType.Layout)
            {
                current.topLevel = cache.topLevel = new GUILayoutGroup();
                current.topLevel.style = style;
                current.topLevel.windowID = windowID;
                if (options != null)
                {
                    current.topLevel.ApplyOptions(options);
                }
                current.layoutGroups.Clear();
                current.layoutGroups.Push(current.topLevel);
                current.windows = cache.windows = new GUILayoutGroup();
            }
            else
            {
                current.topLevel = cache.topLevel;
                current.layoutGroups = cache.layoutGroups;
                current.windows = cache.windows;
            }
        }

        [SecuritySafeCritical]
        private static GUILayoutGroup CreateGUILayoutGroupInstanceOfType(Type LayoutType)
        {
            if (!typeof(GUILayoutGroup).IsAssignableFrom(LayoutType))
            {
                throw new ArgumentException("LayoutType needs to be of type GUILayoutGroup");
            }
            return (GUILayoutGroup) Activator.CreateInstance(LayoutType);
        }

        internal static GUILayoutGroup DoBeginLayoutArea(GUIStyle style, Type LayoutType)
        {
            return BeginLayoutArea(style, LayoutType);
        }

        private static Rect DoGetAspectRect(float aspect, GUIStyle style, GUILayoutOption[] options)
        {
            EventType type = Event.current.type;
            if (type != EventType.Layout)
            {
                if (type == EventType.Used)
                {
                    return kDummyRect;
                }
                return current.topLevel.GetNext().rect;
            }
            current.topLevel.Add(new GUIAspectSizer(aspect, options));
            return kDummyRect;
        }

        private static Rect DoGetRect(GUIContent content, GUIStyle style, GUILayoutOption[] options)
        {
            //GUIUtility.CheckOnGUI();
            EventType type = Event.current.type;
            if (type != EventType.Layout)
            {
                if (type == EventType.Used)
                {
                    return kDummyRect;
                }
                return current.topLevel.GetNext().rect;
            }
            if (style.isHeightDependantOnWidth)
            {
                current.topLevel.Add(new GUIWordWrapSizer(style, content, options));
            }
            else
            {
                Vector2 vector = style.CalcSize(content);
                current.topLevel.Add(new GUILayoutEntry(vector.x, vector.x, vector.y, vector.y, style, options));
            }
            return kDummyRect;
        }

        private static Rect DoGetRect(float minWidth, float maxWidth, float minHeight, float maxHeight, GUIStyle style, GUILayoutOption[] options)
        {
            EventType type = Event.current.type;
            if (type != EventType.Layout)
            {
                if (type == EventType.Used)
                {
                    return kDummyRect;
                }
                return current.topLevel.GetNext().rect;
            }
            current.topLevel.Add(new GUILayoutEntry(minWidth, maxWidth, minHeight, maxHeight, style, options));
            return kDummyRect;
        }


        internal static void EndLayoutGroup()
        {
            EventType type = Event.current.type;
            current.layoutGroups.Pop();
            current.topLevel = (GUILayoutGroup) current.layoutGroups.Peek();
        }
        internal static void Layout()
        {
            if (current.topLevel.windowID == -1)
            {
                current.topLevel.CalcWidth();
                current.topLevel.SetHorizontal(0f, Mathf.Min((float) Screen.width, current.topLevel.maxWidth));
                current.topLevel.CalcHeight();
                current.topLevel.SetVertical(0f, Mathf.Min((float) Screen.height, current.topLevel.maxHeight));
                LayoutFreeGroup(current.windows);
            }
            else
            {
                LayoutSingleGroup(current.topLevel);
                LayoutFreeGroup(current.windows);
            }
        }

        internal static void LayoutFreeGroup(GUILayoutGroup toplevel)
        {
            foreach (GUILayoutGroup group in toplevel.entries)
            {
                LayoutSingleGroup(group);
            }
            toplevel.ResetCursor();
        }

        internal static void LayoutFromEditorWindow()
        {
            current.topLevel.CalcWidth();
            current.topLevel.SetHorizontal(0f, (float) Screen.width);
            current.topLevel.CalcHeight();
            current.topLevel.SetVertical(0f, (float) Screen.height);
            LayoutFreeGroup(current.windows);
        }

        internal static float LayoutFromInspector(float width)
        {
            if ((current.topLevel != null) && (current.topLevel.windowID == -1))
            {
                current.topLevel.CalcWidth();
                current.topLevel.SetHorizontal(0f, width);
                current.topLevel.CalcHeight();
                current.topLevel.SetVertical(0f, Mathf.Min((float) Screen.height, current.topLevel.maxHeight));
                float minHeight = current.topLevel.minHeight;
                LayoutFreeGroup(current.windows);
                return minHeight;
            }
            if (current.topLevel != null)
            {
                LayoutSingleGroup(current.topLevel);
            }
            return 0f;
        }

        private static void LayoutSingleGroup(GUILayoutGroup i)
        {
            if (!i.isWindow)
            {
                float minWidth = i.minWidth;
                float maxWidth = i.maxWidth;
                i.CalcWidth();
                i.SetHorizontal(i.rect.x, Mathf.Clamp(i.maxWidth, minWidth, maxWidth));
                float minHeight = i.minHeight;
                float maxHeight = i.maxHeight;
                i.CalcHeight();
                i.SetVertical(i.rect.y, Mathf.Clamp(i.maxHeight, minHeight, maxHeight));
            }
            else
            {
                i.CalcWidth();
                Rect rect = Internal_GetWindowRect(i.windowID);
                i.SetHorizontal(rect.x, Mathf.Clamp(rect.width, i.minWidth, i.maxWidth));
                i.CalcHeight();
                i.SetVertical(rect.y, Mathf.Clamp(rect.height, i.minHeight, i.maxHeight));
                Internal_MoveWindow(i.windowID, i.rect);
            }
        }

        internal static LayoutCache SelectIDList(int instanceID, bool isWindow)
        {
            LayoutCache cache;
            Dictionary<int, LayoutCache> dictionary = !isWindow ? storedLayouts : storedWindows;
            if (!dictionary.TryGetValue(instanceID, out cache))
            {
                cache = new LayoutCache();
                dictionary[instanceID] = cache;
            }
            current.topLevel = cache.topLevel;
            current.layoutGroups = cache.layoutGroups;
            current.windows = cache.windows;
            return cache;
        }

        internal static GUIStyle spaceStyle
        {
            get
            {
                if (s_SpaceStyle == null)
                {
                    s_SpaceStyle = new GUIStyle();
                }
                s_SpaceStyle.stretchWidth = false;
                return s_SpaceStyle;
            }
        }

        internal static GUILayoutGroup topLevel
        {
            get
            {
                return current.topLevel;
            }
        }

        internal sealed class LayoutCache
        {
            internal GenericStack layoutGroups;
            internal GUILayoutGroup topLevel;
            internal GUILayoutGroup windows;

            internal LayoutCache()
            {
                this.topLevel = new GUILayoutGroup();
                this.layoutGroups = new GenericStack();
                this.windows = new GUILayoutGroup();
                this.layoutGroups.Push(this.topLevel);
            }

            internal LayoutCache(GUILayoutUtility.LayoutCache other)
            {
                this.topLevel = new GUILayoutGroup();
                this.layoutGroups = new GenericStack();
                this.windows = new GUILayoutGroup();
                this.topLevel = other.topLevel;
                this.layoutGroups = other.layoutGroups;
                this.windows = other.windows;
            }
        }
    }
}

